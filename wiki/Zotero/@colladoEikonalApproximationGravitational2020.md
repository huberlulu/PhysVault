
---
title: The Eikonal Approximation and the Gravitational Dynamics of Binary Systems
authors: Arnau Koemans Collado
year: 2020
contained in: arXiv:2009.11053 [hep-th]

DOI: 
url: 

---
# [The Eikonal Approximation and the Gravitational Dynamics of Binary Systems](zotero://select/items/@colladoEikonalApproximationGravitational2020)
