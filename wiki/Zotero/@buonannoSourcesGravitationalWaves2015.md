
---
title: Sources of Gravitational Waves: Theory and Observations
authors: Alessandra Buonanno, B. S. Sathyaprakash
year: 2015
contained in: arXiv:1410.7832 [gr-qc]

DOI: 
url: 

---
# [Sources of Gravitational Waves: Theory and Observations](zotero://select/items/@buonannoSourcesGravitationalWaves2015)
