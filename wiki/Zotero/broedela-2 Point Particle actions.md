* Mdnotes File Name: [[broedela]]

# 2 Point Particle actions ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=10&annotation=26EYGIUW))

>generalize point-particle actions in several ways”  ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=10&annotation=DSSMEBNG))

>2.1 From non-relativistic to proper relativistic actions”  ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=10&annotation=MSWHA2Z8))

>Non-relativistic particle:”  ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=10&annotation=GTCKMG84))

^56c783

>non-relativistic point particle at position ~x (t)–inotherwords,thetime is used as parameter”  ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=10&annotation=5WKM36SC))

^d347c6

>non-relativistic point particle at position ~x (t)”  ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=10&annotation=FRVMRHBF))

\[image\] ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=10&annotation=QH2WGP63))   ^b4f5f8


\[image\] ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=10&annotation=BBT6524C))   ^1c2c3c


>(e.o.m.) are just ̈ ~x (t)=0”  ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=10&annotation=DW3SJYPH))

^566a6f

\[image\] ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=10&annotation=BF5T7URT))   ^3d9ceb


>Relativistic particle:”  ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=10&annotation=DMKFB66D))

^b56027

>Physics, however, should (and does) not depend on the formulation chosen.”  ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=10&annotation=4D6382GJ))

^721288

>simplest Lorentz-invariant quantity is the so-called line-element of the worldline”  ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=10&annotation=I9NZZ4QQ))

^229921

\[image\] ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=10&annotation=SSG532K9))  


>$
-\mathrm{d} s^{2}=-c^{2} \mathrm{~d} t^{2}+\left(\mathrm{d} x^{1}\right)^{2}+\left(\mathrm{d} x^{2}\right)^{2}+\left(\mathrm{d} x^{3}\right)^{2}
$ ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=10&annotation=H7UBWLJF))   ^1cbe3b


>$
\mathrm{d} s=c \mathrm{~d} t \sqrt{1-\frac{\dot{\vec{x}}^{2}}{c^{2}}}
$ ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=11&annotation=GAJVDX39))   ^419390


>$
L=-m c \sqrt{c^{2}-\dot{\vec{x}}^{2}}=-m c^{2}+\frac{1}{2} m \dot{\vec{x}}^{2}+\frac{1}{8} m c^{-2} \dot{\vec{x}}^{4}+\ldots
$ ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=11&annotation=V5E6KFVH))  

^cc1768


>expansion of the square root has an irrelevant constant term”  ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=11&annotation=YQX8ZAHM))

^a832b1

>implies the following e.o.m”  ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=11&annotation=GZTK45AF))

^5a993a

>$
\left(c^{2}-\dot{\vec{x}}^{2}\right) \ddot{\vec{x}}+(\dot{\vec{x}} \cdot \ddot{\vec{x}}) \dot{\vec{x}}=0
$ ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=11&annotation=GSE8SDGD))   ^a3674a


>implies collinearity of the first and second derivative, ̈ ~x = ↵ ̇ ~x”  ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=11&annotation=JQ9LS9YP))

^4729f3

\[image\] ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=11&annotation=P2U5IQJB))   ^ab2473


>Worldline action for a relativistic point particle:”  ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=11&annotation=H2IP8C9M))

^7575db

>the spacetime coordinates are still expressed in terms of the time t,whichisdefinedinaparticularLorentzfram”  ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=11&annotation=YPWMXV4V))

^612462

>goal is to find a formulation in terms of 4-vectors Xμ(⌧ )=(ct, ~x)andPμ =(E/c, ~ p)”  ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=11&annotation=E3WMKQJZ))

>set c = 1”  ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=11&annotation=KTAADMVF))

>proper time s of the particle’s path Xμ(t(⌧ )) in spacetime (worldline)”  ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=11&annotation=7G8YF5H3))

>depends on the location of the worldline only, but not on a particular Lorentz frame (definition of t)orparametrisationoftheworldline(throught)”  ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=11&annotation=YR5M54SB))

>parameterizedbyacurveparameter⌧”  ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=11&annotation=ZSK4DDDT))

\[image\] ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=11&annotation=7GHRQN3D))  


>a dot denotes d/d⌧ .”  ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=11&annotation=ZBZUJ6D5))

>manifestly relativistic formulation.”  ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=11&annotation=S3YJFLIM))

>there is the new function X0(⌧ )=t(⌧ )”  ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=11&annotation=CMFRKMJQ))

\[image\] ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=11&annotation=NK57CX24))  


>equations of motion for the new action read”  ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=12&annotation=6FBD776K))

\[image\] ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=12&annotation=BVZIB4NJ))  


>imply collinearity ̈ Xμ = ̇ Xμ for all ⌧ with variable (⌧ ): the particle moves on a straight line”  ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=12&annotation=W2BERVML))

>Momenta are obtained as derivatives of L = m p ̇ X2 w.r.t. ̇ Xμ”  ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=12&annotation=77EQ9ATA))

\[image\] ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=12&annotation=7JFDUDEX))  


>squares to m2 and thus delivers the mass shell condition P 2 = m2”  ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=12&annotation=NV7ENCLS))

>three independent Pμ,andfourindependentXμ”  ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=12&annotation=ZK3LH6E4))

>reparametrization⌧ 0 = f (⌧ )doesnotchangethephysic”  ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=12&annotation=YQDJ9LX5))

>redundant description in terms of the worldline coordinate ⌧”  ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=12&annotation=JYCZE4GQ))

^764d85

>gauge invariance e↵ectively removes one Xμ,e.g.timeX0 = t(⌧ )”  ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=12&annotation=M5N3XZWT))

^52d94c

>the naive Hamiltonian is strictly zero: H =0”  ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=12&annotation=6TF7EU9W))

>fully relativistic formulation featuring gauge invariance: this complicates the formulation. It is, however, an advantage as well, as it allows to access the symmetries of the theory easier”  ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=12&annotation=ZMR3R69X))

^e7718d

>not polynomial”  ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=12&annotation=W9TAP3Y2))

^c8085d

>complicatesquantization(later”  ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=12&annotation=QNT96G6U))

>does not work for massless particles”  ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=12&annotation=G4WF5468))

^79cb2b

>removedbyintroducinganauxiliaryvariablee(⌧ ):”  ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=12&annotation=XL53JX74))

\[image\] ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=12&annotation=S5L7FN3P))   ^225814


>equations of motion”  ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=12&annotation=6Z6EC98E))

^9f611d

\[image\] ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=12&annotation=EID77PAJ))  


>momentum conjugate to Xμ reads Pμ = e1 ̇ Xμ”  ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=12&annotation=Z8CNZLAD))

>equationofmotionfore reduces to P 2 = m2”  ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=12&annotation=Q897UIUY))

>momentum conjugate to e vanishes, signalling a constraint.”  ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=12&annotation=K3KRJQBV))

>m =0isavalidchoice at every step of the above derivation”  ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=12&annotation=YWMN5HSU))

^045308

>constant momentum P μ = e1 ̇ Xμ as well as P 2 = 0”  ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=12&annotation=4JSXCSN4))

^79455c

>e is not fixed by the e.o.m.: there is a remaining gauge freedom”  ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=12&annotation=2IJQJTTH))

^5e1b66

>einbein e is a dynamical variable”  ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=13&annotation=B97JVL4L))

>einbein specifies a metric g⌧⌧ = e2 on the worldline”  ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=13&annotation=V7IMVYIS))

^017c29

\[image\] ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=13&annotation=EKK3Q7HC))  


>g⌧⌧ =(g⌧⌧)1 = 1/e2.”  ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=13&annotation=P5XAM4DC))

>Various gauge choices:”  ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=13&annotation=FRBJF3V9))

>freedom to either fix one of the coordinates Xμ(⌧ )or the auxiliary field e”  ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=13&annotation=A4THSD7Q))

>Temporal Gauge. t(⌧ )=⌧ or t(⌧ )=↵⌧”  ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=13&annotation=95XS9B9J))

^65d9df

>Reduces to non-relativistic treatment of the beginning of the section.”  ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=13&annotation=H2MDP5PI))

^bbda9e

>Spatial Gauge. z(⌧ )=↵⌧ .”  ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=13&annotation=7JGCBYYW))

^9c74e9

>Works lo cally except at turning p oints of z(⌧ ).”  ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=13&annotation=3CW7C8YW))

>Light Cone Gauge. x+(⌧ ):=t(⌧ )+z(⌧ )=↵⌧ .”  ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=13&annotation=PKE3U967))

^eaa622

>Useful in some cases; prominent in string theory”  ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=13&annotation=SAKNBWQH))

^bc85d3

>Proper Time Gauge. ds =d⌧”  ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=13&annotation=HP3ZAWG8))

^232f84

>Fixes t(⌧ )throughtheintegra”  ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=13&annotation=3VBQ53VY))

^01a5c5

\[image\] ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=13&annotation=PYR4B8EM))  


>Action becomes trivial S = R d⌧ ;dynamicsgovernedbyconstraint.”  ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=13&annotation=NWS4WPXW))

^e471e0

>Constant Einbein. ̇ e =0.”  ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=13&annotation=KWIVQ5BP))

^42aa1e

>gauge fixing may involve e”  ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=13&annotation=R6YLFBPY))

>e.o.m.reducesto”  ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=13&annotation=KV2KBC6B))

\[image\] ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=13&annotation=D5QTTJRX))  


\[image\] ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=13&annotation=B92S36AN))  


>turns into a constraint”  ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=13&annotation=BH54MB2I))

^72f30d

>2.2 Interactions”  ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=14&annotation=TSG8IVRE))

>we need interacting theories”  ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=14&annotation=Z5Z52D8E))

>extension of the polynomial action, where the free relativistic field is coupled to electrical and gravitational fields”  ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=14&annotation=B25PS2WN))

^ca215a

\[image\] ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=14&annotation=FZI4JXPI))  


>Aμ is the potential for the electromagnetic field”  ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=14&annotation=85WLZB8X))

^ccc3f1

>Fμ⌫ = @μA⌫ + @⌫Aμ the corresponding field strength”  ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=14&annotation=8LARZZKI))

^6d7326

>gμ⌫ is the gravitational potential”  ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=14&annotation=EXMH64CD))

>Aμ and gμ⌫ are assumed to be fixed external fields”  ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=14&annotation=A6VC7S2H))

^865c7a

>fields are, however, evaluated at the dynamical position Xμ(⌧ )”  ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=14&annotation=AKGR7WQ6))

^ede445

>assumes weak interactions, which allows to work with free quantum fields formally. Interactions are then introduced in a perturbative fashion.”  ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=14&annotation=MT9TALB4))

^72fc36

>Interactions between particles”  ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=14&annotation=V4GNJD23))

>is taken care of by introducing vertices where several particle worldlines meet:”  ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=14&annotation=E86UFVBE))

^71f7c9

>not the standard treatment of particle interactions in quantum field theory (QFT)”  ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=14&annotation=M69RIZ64))

^044b68

>QFT, an interaction of n fields corresponds to a term n in QFT action usually”  ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=14&annotation=EERRYT59))

^ed6dc3
