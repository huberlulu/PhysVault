* Mdnotes File Name: [[broedela]]
- ([Broedel](zotero://select/library/items/8CFKRHKS)) 
# 3 Classical Bosonic String ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=15&annotation=RWXHNY2Y))

>classical actions for the string: ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=15&annotation=F49WK8NW))

>one-dimensional extended object without inner structure, but with the possibility to oscillate ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=15&annotation=UZ886VED))

^024bce

>parameter space: strings will swipe out a two-dimensional worldsheet on which the dynamics in the embedding space (or ambient space) will be modelled ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=15&annotation=VY8T8IB2))

^652893

>bosonic string theory, ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=15&annotation=EJIJI5J5))

>bosonic string theory ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=15&annotation=84928UUS))

>worldsheet coordinates:  $\xi^\alpha=(\tau,\sigma)$, time $\tau$, space $\sigma$, with $\alpha \in \{0,1\}$([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=15&annotation=CFEIHCJ3))

>embedding coordinates: string map: (⌧, ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=15&annotation=8HWE8SXT))

>D-dimensional embedding: Minkowski space, metric ⌘μ⌫ ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=15&annotation=ZPK9HPVW))

>3.1 Nambu–Goto Action ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=15&annotation=23PW7ZSF))

>Nambu–Goto action is the string generalization of the worldline action described in the last chapter ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=15&annotation=ECQUEFDF))

^8a2d52

>worldline action was proportional to the proper time ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=15&annotation=M4SEEX4N))

^b224fe

>Nambu–Goto action is simply the area of the world sheet ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=15&annotation=8BV2C9SS))

^65b741

>worldline ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=15&annotation=7369BVI5))

>action ' >length” ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=15&annotation=R4KPD2UI))

>Area and Action. ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=15&annotation=9QZK4QEL))

>area element in terms ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=15&annotation=9RK45QRR))

>f the embedding coordinates Xμ reads ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=16&annotation=ETRB62FJ))

^00a026

>Wick rotation t = ̊ıw ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=16&annotation=372NNNW3))

\[image\] ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=16&annotation=DAW2EM4B))  


>pull back of the spacetime metric ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=16&annotation=HD4PVYT9))

^c3bdfc

\[image\] ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=16&annotation=2QK6BGS6))  


>Nambu–Goto action ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=16&annotation=3NRPPQM2))

>exhibits the following symmetries ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=16&annotation=97YZ3D32))

>Lorentz symmetry: because the action is built from scalar products of Lorentz vectors X, ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=16&annotation=5ZK2HPR9))

>Poincar ́e symmetry: as there is no explicit dependence on X,onlythroughderivatives @X, ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=16&annotation=PAGMW6U9))

>worldsheet di↵eomorphisms: since the density d⇠2 p ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=16&annotation=LJMI6QK4))

>Tension. ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=16&annotation=K9TG4JJ7))

>parameter↵0 is related to the fundamental string length scale ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=16&annotation=EARDXJU5))

^7baeb5

>Considering a time slice of the action (and thus a slice of the worldsheet). ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=16&annotation=JIEBVE9W))

>A slice of length L leads to a potential U ⇠ L/2.Theconstantforceresultingfromthepotentialisthe string tension: T = U 0 =1/2⇡2 ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=16&annotation=SWSIWGWF))

^ce6098

>this tension is constant: a spring or a rubber band would have a varying tension. ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=16&annotation=AYAFUNHL))

^d55a1e

>Equations of Motion ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=16&annotation=SXMCIAX6))

>variation of the determinant ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=16&annotation=6HQN9Q3N))

^108f58

\[image\] ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=16&annotation=HZ7BBBCD))  


>highly non-linear and thus di ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=17&annotation=ZY6H5HKJ))

>geometrically (in euclidean signature)? The stationary action demands a surface with minimal area, i.e. a static soap film. Thus, the mean curvature is zero everywhere which implies that every point of the surface is a saddle point. ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=17&annotation=TJWTL4NV))

>3.2 Polyakov Action ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=17&annotation=H9538LR8))

^e76a0e

>polynomial action with an additional dynamical worldsheet metric g↵ ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=17&annotation=I4MPV6KZ))

\[image\] ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=17&annotation=3K2RA5PM))  


>intrinsic worldsheet metric g is the stringy analogue of the einbein e for the point particle, which was considered as the (one-component) metric g⌧⌧ = ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=17&annotation=Y24363Y6))

^3687c1

>g is a symmetric 2-tensor on the worldsheet. A priori it is independent of the pullback metric ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=17&annotation=59JY4D9Z))

^70afb4

>curved.Thatis,thereisaLevi-Civita-connectionand covariant derivative on the worldsheet. For a scalar field (Xμ is a worldsheet scalar), partial and covariant derivative agree. ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=17&annotation=2N8Z4NKY))

^ae356b

>e.o.m. for the field X is the same as above ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=17&annotation=DNM52UTF))

^0ee739

>e.o.m. for the worldsheet metric g reads: ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=17&annotation=VV97C6BP))

^4cdcb4

\[image\] ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=17&annotation=B6PIDQGH))  


>elates the dynamical worldsheet metric to the induced metric from the Nambu–Goto action ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=17&annotation=UH5X59ZL))

^80a893

\[image\] ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=17&annotation=F4K4AUV6))  


>The new redundancy introduced by describing the string with the help of the dynamical worldsheet metric is called Weyl invariance ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=17&annotation=SM2LKZWH))

\[image\] ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=17&annotation=VJ3DQRZP))  


>3.2.1 Conformal gauge and energy-momentum tensor ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=17&annotation=MVM5KJI9))

>gauge freedom in order simplify the e.o.m. for X ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=17&annotation=Z4ZQ7VT9))

>demand a conformally flat metric: g↵ ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=17&annotation=VLS4SE9B))

^e97991

\[image\] ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=18&annotation=5GZGPFP8))  


>fixing the metric to be conformally flat, we have been using all di↵eomorphisms on the worldsheet which preserve the metric up to scale f ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=18&annotation=WXFRDEPS))

>remainingdi↵eomorphismsare the Weyl scalings ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=18&annotation=9ZHETZJD))

>set f = 1 and thus fix the Weyl redundancy as well ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=18&annotation=2BG7K4IC))

\[image\] ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=18&annotation=DCAP9UQZ))  


>corresponding equation of motion for X is simply the harmonic wave equation: ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=18&annotation=5JBVJMRZ))

^ad765e

\[image\] ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=18&annotation=5YXGKIIA))  


>equation of motion for the worldsheet metric ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=18&annotation=WG7JBT9F))

^7f20e1

\[image\] ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=18&annotation=X9KF2XMX))  


>T↵ ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=18&annotation=8RSYXRJ4))

>trace of the energymomentum tensor vanishes by construction due to the conformal/Weyl symmetry (T ↵ ↵ =0), ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=18&annotation=L7FA76BV))

^57dd9e

>two remaining e.o.m. are called Virasoro constraints ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=18&annotation=94APYCGA))

>Virasoro constraints ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=18&annotation=ABA8EF4L))

\[image\] ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=18&annotation=K7LUUYBY))  


>first constraint demands the lines of constant ⌧ to be orthogonal to lines of constant ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=18&annotation=AYBHFZYS))

^673caf

>the string does not have an inner structure! ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=18&annotation=H5YVJ2B6))

^2bf2c3

>energy-momentum tensor T↵ ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=18&annotation=RX9Q5R2T))

>symmetries of the Polyakov action explicitly: ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=18&annotation=CAR3SY8X))

>Spacetime symmetries: ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=18&annotation=UQSLAZPX))

>D-dimensional Poincar ́e symmetry: ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=18&annotation=TSED8795))

^09b578

>Xμ(⇠) 7! ⇤μ⌫X⌫(⇠)+V μ with ⇤ 2 SO(1,D ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=18&annotation=QVUIXAJF))

>Worldsheet symmetries: ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=18&annotation=W36S8YMM))

>Local di↵eomorphism invariance: ⇠↵ 7! ̃ ⇠↵ = ⇠↵ ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=18&annotation=URGNEH28))

^b320ec

>Local conformal invariance (Weyl invariance): g↵ ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=18&annotation=XV7U3ZBI))

^36b649

>3.3 Light cone variables on the worldsheet ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=18&annotation=GUBI3BTS))

>harmonic wave equation (3.14) can be most easily solved ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=18&annotation=8USBPVVL))

>light cone coordinates ⇠L/R: ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=18&annotation=4YPTDR6V))

\[image\] ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=18&annotation=EEHDVDYQ))  


\[image\] ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=19&annotation=CF7XWKEX))  


\[image\] ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=19&annotation=PEQXZDP6))  


\[image\] ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=19&annotation=8AIV5K4R))  


>D left-movers XL and D right-movers XR initially, the Virasoro constraints (@XR,L)2 =0removeoneleft-andoneright-mover ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=19&annotation=QZTC9C5Z))

>conformal transformations: ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=19&annotation=DFVTTWVY))

\[image\] ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=19&annotation=9379RTI8))  


>remove another left- and right-mover ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=19&annotation=MTD6CB9F))

>there are (D ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=19&annotation=BU5GJHGI))

>3.4 Solutions for the closed string ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=19&annotation=XNNIZPUR))

>worldsheets have been extended infinitely in space and time ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=19&annotation=28JVVRDP))

>two possible basic topologies for a string ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=19&annotation=7KLGRKRM))

>closed string: circular topology, identify ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=19&annotation=X6KTKEAY))

>open string: interval topology, boundary conditions at ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=19&annotation=P6X4CCHR))

>3.4.1 Covariant formulation (ambient space) ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=19&annotation=YKHD6W9D))

>For the closed string, the solution function X should be 2⇡-periodic ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=19&annotation=RPM4NDEH))

^aa2a07

>Fourier decomposition: ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=19&annotation=FK4JZAJ5))

^aff9cb

\[image\] ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=19&annotation=B4484UKZ))  


>normalisation for the coecients i/p2 n are chosen for later convenience ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=20&annotation=2MKNRNNB))

^d32b3c

>Linear dependency on ⇠L/R does not clash with the periodicity condition ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=20&annotation=ZUH6R93L))

^a18e07

>RealityoftheembeddingcoordinateX is ensured by demanding ↵n =(↵n)⇤. ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=20&annotation=BNK2CLFN))

^12e07c

>motion of the centre of mass is described by the conjugate pair x, p (conjugation involves an additional factor of 2 ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=20&annotation=I5Z3MGMC))

>the string modes ↵L/R,μ n (left/right movers) describe the amplitudes of the oscillations on the string. ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=20&annotation=2GHQF34L))

>Plugging the above ansatz into the Virasoro constraints (@L/RXμ L/R)2 =0 ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=20&annotation=NYR89SCA))

\[image\] ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=20&annotation=UWVYLTUS))  


>Virasoro modes ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=20&annotation=5E5URB4D))

\[image\] ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=20&annotation=YK6JZT8M))  


>↵L 0 = ↵R 0 = p/p2 ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=20&annotation=NZPMUAFN))

>· signifies a Lorentz product, that is, a contraction of the μ, ⌫-indices with the ambient space metric ⌘μ⌫. ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=20&annotation=8XH86X44))

>validity of the Virasoro constraints ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=20&annotation=2HWMQ8MK))

\[image\] ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=20&annotation=5S39VNY5))  


>L0 = 0 equals the on-shell condition ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=20&annotation=IC38X5JK))

>mass of the string: ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=20&annotation=XHE7H48V))

\[image\] ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=20&annotation=29NZDJKH))  


>all Virasoro constraints Ln =0areconservedbythee.o.m. ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=20&annotation=4FNSU99S))

\[image\] ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=20&annotation=HWIEWURV))  


>impose them on an initial time slice only. ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=20&annotation=DCXJQ7BV))

>mass of a string depends on the mode amplitudes ↵. ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=20&annotation=4GQZ9T75))

>noneofthemodesisexcited, the string behaves like a massless point particle. ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=20&annotation=UTGVDLN2))

>there are only few and small excitations, one will encounter a light (or tiny) particle ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=20&annotation=STKFS9IN))

>large excitations can add up to yield a big and highly massive object. ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=20&annotation=NPGYWPSH))

>Minkowski signature of the spacetime, the time-like modes ↵0 contribute a negative M 2 ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=20&annotation=FGWWR3XH))

>takes all Virasoro constraints into account, tachyons (particles with negative mass-squared) are excluded. ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=20&annotation=NX7JN7R4))

>tachyons (particles with negative mass-squared) ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=20&annotation=R7QFL8GT))

>3.4.2 Light cone gauge (ambient space) ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=21&annotation=8W9LUTA4))

>Virasoro constraints in their above form are complicated and non-linear ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=21&annotation=MAME42SZ))

>employ the conformal symmetry on the worldsheet in order to solve them ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=21&annotation=NPBYEGIR))

>connection between the conformal symmetry on the worldsheet and the ability to choose the particular gauge in light cone coordinates below can not spelled out here explicitly. ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=21&annotation=4A39MZ4S))

>light cone coordinates in spacetime ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=21&annotation=RW548SW5))

\[image\] ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=21&annotation=A8PQ2PLB))  


>X denotes the transverse components 1 ...(D ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=21&annotation=ZK36MHI6))

>selects a gauge, in which the coordinate X+ depends on ⌧ linearly, i.e. does not exhibit oscillator modes. ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=21&annotation=T87TRK8J))

\[image\] ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=21&annotation=65ZVUM5V))  


\[image\] ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=21&annotation=7RJI6FWI))  


>Virasoro constraint (@ ~ XL/R)2 ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=21&annotation=UHDE3V4F))

\[image\] ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=21&annotation=R9CSJJW3))  


>Periodicity. ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=21&annotation=62GR5JW4))

>periodicityofX+ and X requires ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=21&annotation=8GHIPY7C))

\[image\] ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=21&annotation=MCYBLNFY))  


>String Modes ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=21&annotation=YPSNXQBQ))

>Imposing the above gauge fixing on X+,X ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=21&annotation=RWAR89CZ))

\[image\] ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=21&annotation=CVX9DDL4))  


>modes ↵ are again determined in terms of ↵+ and the modes ~↵ . ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=21&annotation=H9P3DG4W))

>Periodicit ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=21&annotation=K8CC84J8))

>↵ L 0 = ~↵ R 0 , ↵R,+ 0 = ↵L,+ 0 , ↵R,0 = ↵L,- ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=21&annotation=IXQ2TULQ))

\[image\] ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=21&annotation=N4H3DMH2))  


>Taking the ab ove conditions into account ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=22&annotation=U82FBEGL))

>the resulting mass is manifestly p ositive ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=22&annotation=RHNQAUJW))

\[image\] ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=22&annotation=IYU5CI2M))  


>the reality condition ↵n =(↵n)⇤ has been used ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=22&annotation=2BJWST5A))

>the light cone gauge comes with manifestly positive mass for all particles and is a very convenient way to get rid of almost all constraints. However, by introducing light cone coordinates, we give up a manifestly Lorentz-invariant formulation. ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=22&annotation=I7L4JL4V))