* Mdnotes File Name: [[broedela]]


# 1 Introduction and Motivation  ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=5&annotation=DULDKGI4))

>1.1 From point particles to strings and beyond - why study strings? ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=5&annotation=KQ7TPSUJ))

>mathematical beauty and structural simplicity are good guiding principles ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=5&annotation=YRH5AN26))

>ven if a beautiful (e.g. very symmetric) theory might not be a valid ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=5&annotation=2LMD7CGC))

>one might be able to learn something ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=5&annotation=6JGHFIPP))

>how to generalize ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=5&annotation=PDWXXYFC))

>string theory is not very well balanced: being in tune with experiment (or even testability) is traded for mathematical and structural complexity and rigor. ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=5&annotation=ZMBUCLNJ))

>constituents of our world as >small” particles of dimension zero, that is, points hovering in >real space”, our (3+1)-dimensional spacetime ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=5&annotation=LP89462T))

>not have an inner structure ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=5&annotation=LNSQ4YEJ))

>Point particles are very well understood ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=5&annotation=KU6XTLB8))

>actions, equations of motion, conserved currents and coupling constants ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=5&annotation=PI3GFA4E))

>live in >real space”, that is, the coordinate position in the description equals the actual position in >our coordinate system” ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=6&annotation=LGDIJPX2))

>(Hidden) symmetries of a problem can be described by employing auxiliary fields ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=6&annotation=MUICK9DI))

>classical or quantum ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=6&annotation=PA24CFNQ))

>instead of describing motion and interactions of an physics object in >real space”, one can divert to a simpler (e.g. geometrically flat) auxiliary parameter space ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=6&annotation=2ZH6VD7J))

>Particles are considered as small strings, one-dimensional objects, which can be open or closed. Open strings correspond to gauge theory, while closed states describe gravitons. ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=6&annotation=NS5KNIXE))

\[image\] ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=6&annotation=LX9K6I3B))  


>fundamental parameter T ,thestring tension,whichdescribeshow easily a string can flex and/or vibrate ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=6&annotation=4GJL389Z))

>Strings can split, join and oscillate ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=6&annotation=VSBLP3SH))

>oscillation contains some energy, ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=6&annotation=Z4C6N476))

>di↵erent masses of particles can be modelled by di↵erent types of oscillations. ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=6&annotation=RYGZMBBX))

>called modes ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=6&annotation=WFL6PQZA))

>strings are referred to as excited,if they oscillate ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=6&annotation=LNAIV8RT))

>two-dimensional parameter space with coordinates ⌧ and ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=6&annotation=HUFEQ2YU))

>target space and its position is described by functions Xμ(⌧, ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=6&annotation=C4L3WA5H))

>moving in spacetime, the string sweeps out a worldsheet ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=6&annotation=HWXRIPEB))

>pointparticle, which leaves a worldline ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=6&annotation=WWGD7WYT))

\[image\] ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=6&annotation=LXJUMTKJ))  


>classical descriptions of strings as well as quantum strings. ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=7&annotation=ZM2HXN3T))

>more >freedom” in a string description ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=7&annotation=7ATIWYQE))

>Symmetries can be realized more eciently ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=7&annotation=9QENHRDE))

>Divergencies and other mathematical imponderabilities ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=7&annotation=56WFQD8X))

>improves substantially, when considering strings. ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=7&annotation=J8QPT8E9))

>Quantized point particle works amazingly well for three out of four (known) forces in our universe: electromagnetism, strong force and weak force. ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=7&annotation=2WD26VHI))

>Field quantisation introduces quanta: EM: photon, strong nuclear forces: gluons, gravity: graviton, matter fields: electrons, quarks, neutrinos. ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=7&annotation=GQQ9MLBC))

>Feynman graphs. These graphs are >pointlike” in nature, which implies a whole complicated formalism of regularization. ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=7&annotation=5MQRIQNQ))

>more complicated Feynman graphs are, the less likely ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=7&annotation=NVLJMWDV))

>Feynman graphs are lab elled by the numb er of lo ops (e.g. closed pats) ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=7&annotation=VXWADGWD))

>finite number of possible interactions and – correspondingly – a finite number of coupling constants. ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=7&annotation=XSYBJGMH))

>fourth known force is lacking a quantum description ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=7&annotation=ZZJ867I6))

>infinitely many allowed interactions, which lets renormalization fail. ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=7&annotation=DUEUIMPZ))

>renormalization is a necessary requirement ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=7&annotation=55FIDDU5))

>free of divergences: in other words, those are finite theories ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=7&annotation=DBZLW8V2))

>one fundamental parameter: the string tension T ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=8&annotation=UM7WJI7A))

>inverse string tension is used: 2⇡↵0 =1/T ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=8&annotation=6G93TWSJ))

\[image\] ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=8&annotation=IJP6JI2X))  


>Couplings – in the sense of interaction strengths in quantum field theory ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=8&annotation=LIG5VP8C))

>their values are fixed by the theory. ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=8&annotation=ZZDT5N8X))

>string coupling gS, which comes with string loops in the so-called genus expansion. ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=8&annotation=KN4TLQQS))

>unification of forces, a unified description. This is believed to happen at very high energies. ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=8&annotation=BCJZ2MFR))

>string theory does not describe nature ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=8&annotation=CQIAAY3F))

>why not consider two-dimensional objects? ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=8&annotation=VD7FGSAL))

>most >applications” implied by string theory is the stringgauge duality (or AdS/CFT correspondence). ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=8&annotation=X48TZHXN))

>supersymmetry, a concept appearing very naturally in string theory, has not been found. ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=8&annotation=5TVRDXYJ))

>c = ~ = e = 1: ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=8&annotation=MK3NZ29K))

>kg ⇠ m1. ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=8&annotation=8YGEZQFF))

\[image\] ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=8&annotation=M2L3ZLTB))  


\[image\] ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=9&annotation=4DAEI8EG))  


\[image\] ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=9&annotation=VQJT77H8))  


\[image\] ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=9&annotation=Z93GJHNL))  


\[image\] ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=9&annotation=NKPJUNI2))  


\[image\] ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=9&annotation=BCMCDLVE))  


\[image\] ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=9&annotation=BN3LMA4U))  


\[image\] ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=9&annotation=GCESLG3B))  


\[image\] ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=9&annotation=B3N2XI46))  


\[image\] ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=9&annotation=223C8W75))  
