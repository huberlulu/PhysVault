# Hamiltonian formalism and Legendre transformations.pdf

## Metadata

* Item Type: [[Webpage]]
* Authors: [[Glenn Rowe]]
* Date Added: [[2022-00-14]]
* URL: [https://physicspages.com/pdf/Quantum%20mechanics/Hamiltonian%20formalism%20and%20Legendre%20transformations.pdf](https://physicspages.com/pdf/Quantum%20mechanics/Hamiltonian%20formalism%20and%20Legendre%20transformations.pdf)
* Cite key: rowec
* Topics: [[String]]
, #zotero, #literature-notes, #reference
* PDF Attachments
	- [Rowe_Hamiltonian formalism and Legendre transformations.pdf](zotero://open-pdf/library/items/VJPEKQJ9) ^d7ebeb


##  Zotero links
* [Local library](zotero://select/items/1_5MKQ2E9V)
* [Cloud library](http://zotero.org/users/7873466/items/5MKQ2E9V)

