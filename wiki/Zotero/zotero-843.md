# Euler-Lagrange equations for particle and field theories.pdf

![[zotero-843-zotero#Metadata]]

Other files:
* Mdnotes File Name: [[zotero-843]]
* Metadata File Name: [[zotero-843-zotero]]

##  Zotero links
* [Local library](zotero://select/items/1_HVHSCDDI)
* [Cloud library](http://zotero.org/users/7873466/items/HVHSCDDI)

## Notes
- 