# Classical particle mechanics.pdf

## Metadata

* Item Type: [[Webpage]]
* Authors: [[Glenn Rowe]]
* Date Added: [[2022-00-14]]
* URL: [https://physicspages.com/pdf/Field%20theory/Classical%20particle%20mechanics.pdf](https://physicspages.com/pdf/Field%20theory/Classical%20particle%20mechanics.pdf)
* Cite key: rowea
* Topics: [[String]]
, #zotero, #literature-notes, #reference
* PDF Attachments
	- [Classical particle mechanics.pdf](zotero://open-pdf/library/items/XM3WN9FT)


##  Zotero links
* [Local library](zotero://select/items/1_YFJQNCP9)
* [Cloud library](http://zotero.org/users/7873466/items/YFJQNCP9)

