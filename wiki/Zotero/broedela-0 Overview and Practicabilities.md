* Mdnotes File Name: [[broedela]]
* ([Broedel, p. 2](zotero://select/library/items/8CFKRHKS))

# 0 Overview and Practicabilities ([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=1&annotation=TXZHP8XS))

>string theory - a physics theory, in which particles are modeled as (tiny) strings([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=1&annotation=9F38YIFL))

>Feynman formalism for calculating scattering amplitudes in a point-particle quantum field theory is herein promoted to (or replaced by) a conformal field theory (CFT) delivering the scattering observables from conformal correlators([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=1&annotation=MMISDW9V))

>developed as a theoretical description for the strong nuclear force, it has evolved into a versatile framework with far-reaching impact on particle physics and mathematics.([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=1&annotation=MP6CTXLY))

>nowadays mostly a playground for understanding and developing models for point-particle quantum field theories.([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=1&annotation=NZH2Y9PF))

>graviton, the carrier of the gravitational force, is very natural (and inevitable)([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=1&annotation=UNF3R63Q))

>only unified quantum description of all known interaction in nature([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=1&annotation=LVGSZ9T8))

>allows to study strongly coupled field theories by describing them as a weakly coupled perturbative string theory in Antide–Sitter (AdS) space([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=1&annotation=EK2J77UH))

>holographic principle([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=1&annotation=X5USU62T))

>famous AdS/CFT-correspondence([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=1&annotation=YN8S4NJM))

>string theory might provide a couple of explanations for features occurring in quantum field theories([pdf](zotero://open-pdf/library/items/E2TKJSGY?page=1&annotation=DTUYAEAU))