# Introduction to String theory Notes

## Metadata

* Item Type: [[Manuscript]]
* Authors: [[Johannes Broedel]]
* Date Added: [[2022-00-14]]
* Cite key: broedel
* Topics: [[String]]
, #zotero, #literature-notes, #reference
* PDF Attachments
	- [Broedel_Introduction to String theory Notes.pdf](zotero://open-pdf/library/items/4EUWDIY4)


##  Zotero links
* [Local library](zotero://select/items/1_L73YC464)
* [Cloud library](http://zotero.org/users/7873466/items/L73YC464)

