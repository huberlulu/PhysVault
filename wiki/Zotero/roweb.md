# Hamilton's equations for relativistic fields.pdf

![[roweb-zotero#Metadata]]

Other files:
* Mdnotes File Name: [[roweb]]
* Metadata File Name: [[roweb-zotero]]

##  Zotero links
* [Local library](zotero://select/items/1_JFWILSYM)
* [Cloud library](http://zotero.org/users/7873466/items/JFWILSYM)

## Notes
- 