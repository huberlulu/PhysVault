Highlights and annotations of [[@bernScalarQEDToy2021]]


# 1 Introduction ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=2&annotation=2J8PVH6E))

#important : The landmark detection of gravitational waves \[1, 2\] has opened a remarkable new window into the Universe that promises major new advances into black holes, neutron stars, and perhaps even provides new insights into fundamental physics. ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=2&annotation=J42EGNTJ)) ^56f6bf

#definition : effective one-body (EOB) formalism \[7\], ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=2&annotation=ZGX37JUA)) ^7e27bb

#definition : numerical relativity \[8–10\] ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=2&annotation=LTDP8R58)) ^ae2631

#definition : self-force formalism \[11, 12\] ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=2&annotation=L27SS5U6))

#definition : post-Newtonian (PN) expansion \[13, 14\] ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=2&annotation=6W2JLHM3))

#definition : effective field theory (EFT) ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=2&annotation=XHE8PHCE))

#definition : nonrelativistic general relativity (NRGR) \[15–26\] ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=2&annotation=H8KIBGI7))

#definition : post-Minkowskian (PM) expansion \[27–45\] ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=2&annotation=ACIMMCNH)) ^09545f

#definition : post-Minkowskian approach is a weak-field expansion in Newton’s constant, G, ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=3&annotation=T2KYWQ6F)) ^e0eddb

#important : maintaining Lorentz invariance and gives results with exact relativistic velocity dependence. ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=3&annotation=CZ483ZRN)) ^3c2880

#important : Calculations of scattering amplitudes have advanced enormously, making this a natural framework for state-of-the-art post-Minkowskian calculations ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=3&annotation=7LNAR29E))

#definition : unitarity method \[59–61\] ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=3&annotation=3AUH7T5F)) ^708137

#definition : gauge and gravity theories \[62–66\] ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=3&annotation=4UKYTD5P)) ^98e3b2

#definition : integration by parts (IBP) \[68–70 ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=3&annotation=QHP3P268))

#definition : differential equations \[71–76\] ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=3&annotation=YW4NSR9Q))

#definition : reverse unitarity \[77–80\] ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=3&annotation=7CLDJZ9F))

#definition : effective field theory (EFT) ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=3&annotation=WPL42M3Q))

#definition : EOB framework \[35, 43\] ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=3&annotation=WIGANRAI))

#important : Scattering amplitudes are the natural realm to describe the hyperbolic motion of classical objects from the asymptotic past to the asymptotic future ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=3&annotation=VUNR9TTY)) ^aa210e

#definition : Kosower, Maybee, and O’Connell (KMOC) \[39\] ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=3&annotation=RWH57IHB))

#definition : allows us to extract classical observables directly from scattering amplitudes and what are essentially unitarity cuts. ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=3&annotation=5S2V7ULV)) ^cccd89

#important : The usefulness of the scattering amplitude framework has been demonstrated through the first construction of the conservative two-body Hamiltonian at O(G3) \[41, 42\], as well as new results at O(G4) \[53, 56\]. There have also been a variety of new results for spin \[101116\], tidal effects \[117–126\], and waveforms \[58\]. ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=3&annotation=5CS5QJR4)) ^087cd9

#important : direct iteration of the equations of motion ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=4&annotation=B3TGKPTF))

Kosower-Maybee-O’Connell formalism to obtain the impulse on two massive charged scalar particles scattering at large impact parameter b from which we extract the scattering angle ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=4&annotation=PP9XEY82))

^b0394b

extract the eikonal phase from the scattering amplitude which allows us to determine the scattering angle. ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=4&annotation=U8VUTEGY)) ^6c0ca1

extracting from the scattering amplitude a radial action that determines the scattering angle, including radiative effects. ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=4&annotation=YYLHD8WZ)) ^376f6d

#important : All three of these approaches for extracting the classical scattering angle match, and agree with the result from the classical approach of Ref. \[90\]. ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=4&annotation=QQVFND2Y)) ^038ace

#important : in electromagnetism we encounter a mass singularity in the scattering angle, which does not cancel between potential and radiative contributions, as it does in the corresponding O(G3) calculation in gravity \[127, 128\] ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=4&annotation=8TAN9VT7)) ^260d9e

#important : interpret this singularity as a breakdown of the classical expansion which requires m2|b|21 ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=4&annotation=YZCQ3JQ2))

# 2 Review of Methods ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=5&annotation=DNWW9CUD))

#definition : O(α3) where α = e2/4π ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=5&annotation=WQTB858Z))

review the kinematic parametrization tailored towards the classical expansion of quantum scattering amplitudes in Subsection 2.1 ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=5&annotation=CYC9W7N5)) ^f4a4b5

generalized unitarity framework to determine the amplitude integrands in Subsection 2.2 ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=5&annotation=XJ69PEE8)) ^d615bb

Subsection 2.3, we telegraphically sketch the applicability of modern collider-physics based integration tools to compute precision-level classical observables with the help of integration-by-parts reduction to a minimal set of master integrals and their evaluation by differential equation methods ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=5&annotation=GBV3ZC8D)) ^52f488

Subsection 2.4 introduces a new concept that allows us to define a radial action in the presence of soft-region radiation effects ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=5&annotation=EDKPS5SW)) ^3dabc1

Subsection 2.5, we briefly summarize the Kosower, Maybee, and O’Connell (KMOC) formalism which allows us to extract the classical electromagnetic impulse, the radiated momentum, and the classical scattering angle up to O(α3). ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=5&annotation=92SRT9MK))

# 2.1 Classical limit of quantum scattering amplitudes–soft and potential region ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=5&annotation=KU2K6E23))

#definition : post-Lorentzian” (PL) expansion ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=5&annotation=WY2KUEQ6)) ^06ee81

#definition : post-Minkowskian expansion in gravity. ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=5&annotation=PRNSYKVN))

#definition : Compton wavelength λc ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=6&annotation=83FGY47A))

#definition : Planck’s constant ~ ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=6&annotation=8QH4TE5W))

#definition : particle mass scale m ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=6&annotation=MB96E4R8))

#definition : typical classical particle size rS or rQ (Schwarzschild radius or classical charge radius) ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=6&annotation=LNNJUZ6I))

#definition : inter-particle separation b. ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=6&annotation=3WAQS6UA))

#definition : classical PM expansion corresponds to the following hierarchy of scales: λc rS b, ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=6&annotation=TUJLYRMI)) ^3427df

#definition : similarly λc rQ b in the PL cas ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=6&annotation=7TBEHCE5))

classical limit posits that the individual particle size is much bigger than the Compton wavelength ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=6&annotation=28VQLZNJ))

#conclusion : regime of large charges: rS/λc 1 ↔ G m2/~ 1, or rQ/λc 1 ↔ e2q2 i /~ 1 ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=6&annotation=FLBQTEZA)) ^684d37

#definition : qi is the electric charge of the classical object in units of e ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=6&annotation=GHIHKNP3))

#important : the PM or PL regime amounts to an expansion in terms of the small ratio rS/b 1 or rQ/b 1 which leads to a well-defined perturbative expansion. ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=6&annotation=6YKF64CE))

scattering of spinless, structureless objects described by massive scalar fields. ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=6&annotation=B3JNXZ9K))

assumed that the masses of the scalars are very heavy and that the momentum transfer |q| ∼ 1/|b| is small in the classical limit, (−q2) m2i ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=6&annotation=5FNPUKG8))

#definition : classical ~ → 0 ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=6&annotation=42IRW854))

#definition : equivalently soft (small |q|) expansion ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=6&annotation=74NBSFPT))

#definition : method of regions \[134\] ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=6&annotation=H5CJWAZ2))

#definition : new vectors pi ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=6&annotation=XBUWCXD2))

#conclusion : orthogonal to the momentum transfer q, pi · q = 0 ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=6&annotation=E9R77QVX))

#definition : momentum transfer q ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=6&annotation=YLVZBMDX))

follows from the on-shell conditions p21 = p24 = m21 and p22 = p23 = m22 ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=6&annotation=E3ZPGYE7))

#definition : soft-masses’ mi ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=6&annotation=DENA4T7S))

#important : the physical scattering region s>(m1 + m2)2, q2<0 remains the same ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=6&annotation=B28JSBUW)) ^35020d

#definition : soft four-velocities of the two black holes uμ i = pμ i /|pi| ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=6&annotation=NUS83WHS))

#conclusion : u2i = 1, ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=6&annotation=Q2NJFH84))

physical scattering in the s-channel ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=6&annotation=TDE7BK25))

#conclusion : y > 1 . ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=6&annotation=CHRINR4W))

#important : the soft velocities ui coincide with the classical four velocities of the massive scalars only up to corrections of O(q). ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=7&annotation=H43JFGHG)) ^1f325c

#important : The KMOC setup directly targets physical observables where this difference is immateria ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=7&annotation=Z47LMNQL))

#important : for the eikonal computations, the O(q) corrections do matter and one has to carefully track them ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=7&annotation=IWDQFFCJ))

#definition : \` schematically represents arbitrary combinations of photon momenta of the form $l_1,l_2,l_1\pm l_2,\dots$ ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=7&annotation=RX6V2VMM)) ^d2fd0c

#definition : 1 (\`−q)2 ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=7&annotation=PWE7XD9C))

#important : matter propagators do have a non-trivial |q| expansion expressed via dimensionless velocity variables ui ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=7&annotation=QNBC28A5))

#important : The matter propagators effectively “eikonalize” and the soft expansion to higher orders in |q| can lead to raised propagator powers ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=7&annotation=VI36WJCS))

# 2.2 Generalized Unitarity and scalar QED scattering amplitudes up to O(α3) ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=7&annotation=SWWM9VDR))

#important : involve the (classical limit of) quantum scattering amplitudes. ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=7&annotation=I5IPCU4U))

#important : EFT matching calculation to a classical two-body potential, ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=7&annotation=9LTDM3ZH))

#important : eikonal approach to classical scattering, ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=7&annotation=2636AXDA))

#important : KMOC framework ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=7&annotation=E8KCT2RK))

#important : is crucial to have at our disposal compact expressions for the relevant (classical parts) of the higher-loop scattering amplitudes ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=7&annotation=BPKLX76Y)) ^5d56b7

#important : chief ingredient in many of these calculations is an efficient way to obtain a scattering amplitude integrand, i.e. an expression of the amplitude before loop integration ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=7&annotation=B5LQJSLI)) ^3d2e7b

#definition : Generalized unitarity \[59–61\] is based on the factorization of amplitudes into simpler gauge-invariant on-shell building blocks which allows to export the simplicity of tree-amplitudes to loop-calculations ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=7&annotation=R8RDUFIJ)) ^c208cb

# 2.2.1 Tree-level amplitudes in scalar QED ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=8&annotation=HEMNRTM2))

#important : main building blocks ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=8&annotation=G8ZZRA43))

#important : on-shell tree-level amplitudes ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=8&annotation=PKAJIWSM)) ^f5b1f0

scattering of two massive charged scalars in scalar QED that have charges eq1,2 and masses m1,2, respectively and interact via the exchange of U (1) gauge bosons, i.e. photons ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=8&annotation=W3B6FHR4)) ^f60c7f

#definition : The Lagrangian for the system is $$\mathcal{L}=-\frac{1}{4} F_{\mu \nu} F^{\mu \nu}+\sum_{i=1}^{2}\left[\left(D_{\mu} \phi_{i}\right)^{\dagger}\left(D^{\mu} \phi_{i}\right)-m_{i}^{2} \phi_{i}^{\dagger} \phi_{i}\right]$$ ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=8&annotation=4B6VIG8W)) ^3269a8

#definition : covariant derivative $D^\mu=\partial^\mu-\iunit\,e\,q_iA^\mu$ ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=8&annotation=2375N95H)) ^covDer

#definition : photon field  $A^\mu(x)$ ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=8&annotation=XBGVXFWP)) ^photonField

#definition : electric charge $q_i$ ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=8&annotation=STLYCKP4)) ^elncharge

#definition : fundamental charge $e$ ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=8&annotation=H5LKL75U)) ^fundCharge

#definition : three-point coupling between the scalars and a photon ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=8&annotation=VQBBXU5J))

all-outgoing convention for the particle momenta ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=8&annotation=BM4R8PWY))

#definition : −i e qi(pa − pb)μ ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=8&annotation=Q3CNQR3D))

#definition : tree-level scattering amplitude between the two charged scalars due to photon exchange ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=8&annotation=G3DFR27B))

#definition : 4e2 q1q2 m1m2 y −q2 ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=8&annotation=HKKIUCJR))

#definition : Compton amplitude for the tree-level scattering of two scalars with two photons ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=8&annotation=SP89Z2DV))

#definition : linearized field-strengths F μν i = εμ i pνi − ενi pμ i ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=9&annotation=GJK6SKEW))

#important : manifestly vanish when εi → pi, so that physical state sums that appear in the cut sewing procedure of generalized unitarity can be performed by the simple substitution (see the discussion in Ref. \[136\]) ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=9&annotation=7IGGUSAG))

#conclusion : compact expression on the second line of Eq. (2.8) ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=9&annotation=K7CK779Y))

momentum conservation to eliminate p ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=9&annotation=BSLCSA9H))

transversality condition εi · pi = 0 of the polarization vectors ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=9&annotation=WLLQS2XP))

#definition : amplitude between three photons and two massive scalars ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=9&annotation=4ZZ5PJIQ))

#definition : \] + (2 ↔ 3) + (2 ↔ 4) ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=9&annotation=NCSXJW9N))

# 2.2.2 One-loop integrand in scalar QED ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=9&annotation=DZ7HVYH7))

# Equipped with the tree-level building blocks, we follow the generalized-unitarity framework \[59–61\] ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=9&annotation=ZFQE6WBL))

ansatz of Feynman-like graphs with associated numerators dictated by the power-counting of scalar QED ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=9&annotation=YGG3L866))

#conclusion : we can write the full integrand in terms of box, triangle, and bubble topologies ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=9&annotation=MHYUTFDX))

#definition : box ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=9&annotation=TP2GHEYS))

#definition : triangle, ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=9&annotation=QMHFZ7YP))

#definition : bubble topologies. ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=9&annotation=4JJ9BRA9))

#condition : We do not graphically distinguish ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=9&annotation=4HSI4PJY))

#condition : particles of mass $m_1$ and $m_2$ that are always associated to the external momenta $ p_1$, $p_4$ ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=10&annotation=P54Z4EM5))

and p2, p3, respectively ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=10&annotation=VWD44CEQ))

power-counting of scalar QED ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=10&annotation=EBT97S4F))

#conclusion : the numerator of the box integral should have a mass-scaling like (pi · pj)2. ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=10&annotation=RV95XJJX))

write the numerator in terms of the following external Lorentz-products ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=10&annotation=AHK4H2JK))

#definition : external Lorentz-products ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=10&annotation=I2X7N3LH))

#conclusion : Every numerator basis element that is proportional to one of the inverse propagators Di corresponds to a contact term ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=10&annotation=DLEX63B8))

#definition : contact term ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=10&annotation=6CE2XR4Q))

compare the cut of the ansatz against the field theory result as determined by the product of tree-level amplitudes summed over the exchanged on-shell states that can cross the cut. ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=10&annotation=V3MX7GWV))

classical, long-range interactions between the heavy scalar particles mediated by photon exchange, ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=10&annotation=L63EIWLV))

#conclusion : never need to consider contact (i.e. short distance) interactions ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=10&annotation=8BENL34D))

#definition : contact (i.e. short distance) interactions ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=10&annotation=H6S9IBLK))

Matching the above field theory cut (i.e. the product of two Compton amplitudes of Eq. (2.8)) with our basis ansatz ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=10&annotation=3HZHI4DI))

#conclusion : relabeling the basic box integrand of Eq. (2.11) with the associated numerator (2.14) ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=10&annotation=5JUMNQDS))

Solving the cut equations and dropping all terms proportional to inverse propagators that correspond to pinches of photon lines ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=10&annotation=E6NLXK4D))

#conclusion : we find ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=10&annotation=L5RJT262))

# 2.2.3 Two-loop integrand in scalar QED ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=11&annotation=DZ5WG6IZ))

first two graphs appear in the conservative sector ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=11&annotation=7RAUNNNR))

three “mushroom” graphs are only relevant for radiative effects. ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=11&annotation=FN7A2XR8))

#important : At two-loops, the cut construction proceeds in a fashion similar to the previous oneloop analysis. ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=11&annotation=2SE82HMW))

#important : each trivalent vertex is associated with one power of momentum in the numerator ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=11&annotation=JLV53EDE))

#important : we fix the numerators by matching against the spanning sets of cuts depicted in Fig. 3 built out of products of the tree-level amplitudes from section 2.2.1 ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=11&annotation=BW3BQGPT))

# 2.3 Soft and potential region expansion, IBP, and differential equations ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=11&annotation=Y2DDN835))

import the explicit values of all soft master integrals supplied in the ancillary files of Ref ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=12&annotation=4KZXFYVJ))

start from the initial integrands and expand them in small |q| which leaves graviton propagators unaffected and linearizes (eikonalizes) all matter propagators. ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=12&annotation=5CL9LJ2F))

#definition : linearizes (eikonalizes) all matter propagators ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=12&annotation=XLQW2HIH))

#definition : conservative physics (i.e. the potential region in the language of the method of regions \[134\]), ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=12&annotation=JY3ZZ2JD))

expands the propagators in a formal small velocity parameter v, where the graviton energy component is suppressed by an extra factor of v compared to the spatial components. ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=12&annotation=KZB7IIII))

one subsequently reduces all resulting integrals to a basic set of so-called master integrals ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=12&annotation=6B23DA85))

#definition : differential equation methods \[71–76\] ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=12&annotation=MEGLQALM))

#definition : reverse unitarity \[77–79\]—a well-known tool from collider physics computations (see e.g. Ref. \[80\]) ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=12&annotation=J4X7RFXK))

# 2.4 Soft radial action and master integral subtraction of classically divergent terms ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=12&annotation=LI8P873S))

#definition : soft region where we only assume that the momentum transfer −q2 m2i , s is much smaller than the masses or the energy of the scattering process. ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=12&annotation=J9XMNP5Q))

these amplitudes then enter either the eikonal formalism or the KMOC framework in order to extract the relevant classical observables from the scattering amplitudes (or certain combinations of scattering amplitudes) ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=12&annotation=8NS7G57L))

amplitudes not only have classical contributions but, at higher orders in perturbation theory, also involve classically divergent (‘super-classical’) terms that are more singular and have to cancel for classically well-defined observables.3 ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=12&annotation=GT8U9BB3))

without ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=12&annotation=I2M9KVKE))

In the conservative sector, \[53\] advocated for an EFT based approach with a particular subtraction scheme of classical iterations that ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=12&annotation=FJHLZDII))

allowed the definition and computation of the radial action Ir ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=13&annotation=N834II4K))

#definition : radial action I ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=13&annotation=EFUFLQUB))

#definition : the radial action is an important quantity, associated with the classical Hamilton-Jacobi equation for the system, from which to extract relevant classical observables. ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=13&annotation=EX4W27AM))

#definition : exponential representation of the S-matrix, S = ei ˆ N ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=13&annotation=A73JCEAC))

#definition : phase, ˆ N ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=13&annotation=J54K5DF8))

#conclusion : from which one can extract classical observables (including radiation) ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=13&annotation=TPW8ZZB7))

due to a relation to the WKB approximation. ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=13&annotation=FTMFQVYQ))

#definition : velocity cuts ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=13&annotation=DL7Y36UT))

Similarly to the eikonal approach, the radial action Ir(J) (and also the phase of the Smatrix ˆ N ) has a perturbative expansion Ir(J) = I(0) r ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=13&annotation=PPAFWGYY))

# In this section, we describe an approach to calculate the classical radial action at a given loop order I(L) r (J) without the need of explicit classically divergent subtractions. ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=13&annotation=WBKNPDQA))

Formally, the perturbative expansion of Eq. (2.18) to a given loop-order L still entails the subtraction of nontrivial exponentiation terms involving two or more lower-order iterations I(L′<L) r to isolate I(L) r itself. ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=13&annotation=NLJUAJTE))

our strategy is to drop integrals with linearized propagators from the boundary conditions and solving the differential equations for the soft integrals subject to the modified “finite” boundary conditions. ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=13&annotation=B4A3VU7V))

#definition : G1,1,1,1,1,1,1,0,0 is the scalar double-box integral with a unit numerator ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=13&annotation=QQ79IUDA))

the Euclidean region we have −1 < x < 0, ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=13&annotation=UAB87R4N))

y = (1 + x2)/(2x) < −1 ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=14&annotation=3WTWYE2A))

By analytic continuation, the value of integral in the Lorentzian region 0 < x < 1, y > 1 is obtained ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=14&annotation=PBQEQJ55))

in the potential region, ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=14&annotation=3XQVU4BA))

#conclusion : the integral cannot be analytically continued between positive and negative values of x, ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=14&annotation=U2ML4J4T))

#conclusion : we directly give the value of the integral for the Lorentzian region 0 < x < 1, y = (1+x2)/(2x) > 1 ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=14&annotation=KHCCXNXI))

#conclusion : in the O(2) term only the π2 part survives, and the O(3) term has become genuinely zero, and not an omission ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=14&annotation=8F45PD5N))

#conclusion : There are 7 pure master integrals for the soft-expanded III diagram in the even-in-|q| sector. ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=14&annotation=7XR35T48))

In the potential region, the boundary condition near the static limit y = 1 ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=14&annotation=C8SPK8T8))

#conclusion : is given in terms of (3 − 2)-dimensional integrals in Eqs. (A.9)-(A.11) of Ref. \[67 ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=14&annotation=UP7S896N))

#definition : The superscript (p) in the equation above indicates that only the potential region is considered. ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=14&annotation=23FGDBPJ))

#definition : 4PM potential-region calculation, ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=14&annotation=QXJYXH5B))

dropping (3−2)-dimensional integrals involving linearized propagators arising from iterations of lower-loop potentials ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=14&annotation=5WJY2QAG))

changing the RHS of Eq. (A.11), reproduced in Eq. (2.23), to zero. ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=15&annotation=CRHRZNKW))

#conclusion : vanishes until O(4), which is beyond the order of needed in the classical calculation ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=15&annotation=2QMLHP3Z))

#conclusion : the real part of the result directly gives the radial action after Fourier transform ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=15&annotation=IBU4YW3R))

# 2.5 KMOC framework for classical conservative and radiative observables ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=15&annotation=5Y7MLZGV))

# we schematically recall aspects of the KMOC framework \[39\] as presented in Ref. \[135\]. We only introduce the relevant final formulae ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=15&annotation=HDU3TRI8))

Gedanken experiment for the scattering of two wavepackets representing massive particles from which the classical limit is carefully taken in order to extract the classical observables of interest ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=15&annotation=PMYDWYF7))

quantum setup, we can measure the change of some observable ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=15&annotation=C5L3A3KN))

#definition : observable ∆O ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=15&annotation=7344XGUM))

#definition : quantum operator O ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=15&annotation=H8CHBTE7))

following the time-evolution of states from the asymptotic past to the asymptotic future. ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=15&annotation=X9XBVTM6))

the asymptotic past ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=15&annotation=BPRUZDQU))

#definition : |in〉, an in quantum state constructed from the superposition of two-particle momentum eigenstates |p1, p2〉in with wavefunctions φi(pi) ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=15&annotation=G8YN5K2Q))

#definition : out state in the asymptotic future, |out〉, that might contain additional particles created during the interaction ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=15&annotation=FQMCENIZ))

∆O is obtained by evaluating the difference of the expectation value of O between in and out states ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=15&annotation=WZUI45KS))

#definition : ∆O = 〈out|O|out〉 − 〈in|O|in〉 . ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=15&annotation=3JK3955A))

the out states are related to the in states by the time evolution operator, ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=16&annotation=TTSYZ4I9))

#definition : S-matrix: |out〉 = S|in〉 ([pdf](zotero://open-pdf/library/items/RSKAXN8Q?page=16&annotation=IBX8SH7I))