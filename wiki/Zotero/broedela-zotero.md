# Introduction to String Theory

## Metadata

* Item Type: [[Manuscript]]
* Authors: [[Dr J Broedel]]
* Date Added: [[2022-00-10]]
* Cite key: broedela
* Topics: [[String]]
, #zotero, #literature-notes, #reference
* PDF Attachments
	- [Brodel_Introduction to String Theory.pdf](zotero://open-pdf/library/items/E2TKJSGY)


##  Zotero links
* [Local library](zotero://select/items/1_8CFKRHKS)
* [Cloud library](http://zotero.org/users/7873466/items/8CFKRHKS)

## Highlights and Annotations

- [[broedela - “0 Overview and Practicabilities” (Brodel, p. 2)]]
- [[broedela - “1 Introduction and Motivation” (Brodel, p. 5)]]
- [[broedela - “2 Point Particle actions” (Broedel, p. 2)]]