# Annotations  
(2022-04-16, 10:25:14 a.m.)



# [Introduction](zotero://open-pdf/0_XIBRM7N7/3)


#important : we must understand  
systematically how to extract the classical result using on-shell quantum-mechanical scat-  
tering amplitudes ([pdf](zotero://open-pdf/library/items/XIBRM7N7?page=4&annotation=TL256QAQ))

#important : Our formalism applies  
to both electrodynamics and gravity. ([pdf](zotero://open-pdf/library/items/XIBRM7N7?page=4&annotation=YISDXCMK)) ^1

#important : restrict to spinless scattering ([pdf](zotero://open-pdf/library/items/XIBRM7N7?page=4&annotation=URAPTNYI))

# [Restoring h](zotero://open-pdf/0_XIBRM7N7/5)

#important : A straightforward and pragmatic approach to restoring all factors of $\hbar$ in an expression is  dimensional analysis. ([pdf](zotero://open-pdf/library/items/XIBRM7N7?page=5&annotation=IE238JXF))

#definition : dimensions of mass and length by \[M\] and \[L\], respectively ([pdf](zotero://open-pdf/library/items/XIBRM7N7?page=5&annotation=QP4UUIDK))  ^dimension

#definition : single-particle  
states by ([pdf](zotero://open-pdf/library/items/XIBRM7N7?page=5&annotation=ZCJDACRH)) ^singleParticleState

#definition $\ket{p}=\sqrt{2E_p} \hat{a}^\dagger_p \ket{0}$ ([pdf](zotero://open-pdf/library/items/XIBRM7N7?page=5&annotation=L4T5I3AC))  ^singlePart


$\ket{p}$ is thus $[M]^{-1}$ ([pdf](zotero://open-pdf/library/items/XIBRM7N7?page=5&annotation=U95I28R4))

 $\hat{\delta}^{(n)}(p) \equiv(2 \pi)^{n} \delta^{(n)}(p)$ ([pdf](zotero://open-pdf/library/items/XIBRM7N7?page=5&annotation=QEL9NABG))  ^delta


$\mel{p'_1 \cdots p'_m}{T}{p_1 \cdots p_n}=\mathcal{A}(p_1 \cdots p_n\rightarrow p'_1 \cdots p'_m)\hat{\delta}(p_1 +\cdots+ p_n- p'_1- \cdots-p'_m)$ ([pdf](zotero://open-pdf/library/items/XIBRM7N7?page=5&annotation=65FG6K85))  ^amplitude


#definition :  wavenumber $\bar{p}$ associated with a  momentum $p$: ([pdf](zotero://open-pdf/library/items/XIBRM7N7?page=6&annotation=PN2UEMNV)) $\bar{p}\equiv \frac{p}{\hbar}$ ([pdf](zotero://open-pdf/library/items/XIBRM7N7?page=6&annotation=47IMTTBV))  ^wavenumber

#condition : consider the scattering of two point-like objects, with momenta $p_{1,2}$, initially separated by a transverse impact  parameter $b_\mu$ ([pdf](zotero://open-pdf/library/items/XIBRM7N7?page=6&annotation=JQX62IG4))

#definition : The impact parameter is transverse in the sense that $p_i \cdot b = 0$ for $i = 1, 2$  ([pdf](zotero://open-pdf/library/items/XIBRM7N7?page=6&annotation=X9S5B2MM)) 

^transverseImpact


#definition : (reduced) Compton wavelengths $\ell_c^{(i)}\equiv  \frac{\hbar}{m_i}$  ([pdf](zotero://open-pdf/library/items/XIBRM7N7?page=6&annotation=MLMDULYB))

point-particle description will be  accurate ([pdf](zotero://open-pdf/library/items/XIBRM7N7?page=6&annotation=L8ARP9T2)) $\sqrt{-b^2}\gg \ell^{(1,2)}_c$ ([pdf](zotero://open-pdf/library/items/XIBRM7N7?page=6&annotation=BNUVAJSL))  

#definition : the spread of  the wavepackets,$\ell_w$ ([pdf](zotero://open-pdf/library/items/XIBRM7N7?page=6&annotation=ACG2EMB4))

The quantum-mechanical expectation values of observables, as we  will discuss, are well-approximated by the corresponding classical ones, when the packet  spreads are in the ‘Goldilocks’ zone, $\ell_c\ll\ell_w\ll\sqrt{-b^2}$ ([pdf](zotero://open-pdf/library/items/XIBRM7N7?page=6&annotation=C4QDNFG4))

#important : When $\hbar \neq 1$, the dimensions of the momenta and masses in the  amplitude are unchanged. ([pdf](zotero://open-pdf/library/items/XIBRM7N7?page=6&annotation=DZ6DWZYZ))

> I would think that the momenta go from \[M\] to \[M\]\[L\]\[T\]^-1, but that is only for c also =1!!

#important : each factor of a coupling is multiplied by an additional factor of $\frac{1}{\sqrt{\hbar}}$ ([pdf](zotero://open-pdf/library/items/XIBRM7N7?page=6&annotation=Y8Z9KTAF))

#important : point particles have momenta which are fixed ([pdf](zotero://open-pdf/library/items/XIBRM7N7?page=7&annotation=BN5LNAR8))

#important : massless particles  
and momentum transfers between massive particles ([pdf](zotero://open-pdf/library/items/XIBRM7N7?page=7&annotation=DAYWZCZX))

#important : wavenumber which we should  
treat as fixed in the limit. ([pdf](zotero://open-pdf/library/items/XIBRM7N7?page=7&annotation=ZSSL8AJ2))

# [Impulse and radiated momentum in quantum field theory](zotero://open-pdf/0_XIBRM7N7/7)

#condition : We examine scattering events in which two widely separated particles are prepared at  
t → −∞, and then shot at each other with impact parameter bµ. ([pdf](zotero://open-pdf/library/items/XIBRM7N7?page=7&annotation=T3VWF5FR))

#condition : We will also restrict  
our attention to scattering processes in which quanta of fields 1 and 2 are both present in  
the final state ([pdf](zotero://open-pdf/library/items/XIBRM7N7?page=7&annotation=ZRGZY4VG))

## [The incoming state](zotero://open-pdf/0_XIBRM7N7/7)

#condition : initial state ([pdf](zotero://open-pdf/library/items/XIBRM7N7?page=7&annotation=S3PHJUIB))

\[image\] ([pdf](zotero://open-pdf/library/items/XIBRM7N7?page=7&annotation=HZT9H9GM))  
([Kosower et al., 2019, p. 5](zotero://select/library/items/LFU9BUYS))

#definition : $\hat{\dd}p$ absorbs a factor of $2 \pi$; more generally $\hat{\dd}^n p$ is defined by $\hat{\dd}^n p=\frac{\dd[n]{p}}{2 \pi}$ ([pdf](zotero://open-pdf/library/items/XIBRM7N7?page=7&annotation=RH372LV7))



$\hat{\delta}^{(+)}(p^2-m^2)=2 \pi \,\Theta(p^0)\,\delta(p^2-m^2)$ ([pdf](zotero://open-pdf/library/items/XIBRM7N7?page=7&annotation=ZGTDDU7E))  


#definition : on-shell integrals (over  
Lorentz-invariant phase space), $\dd{\Phi(p_i)}=\hat{\dd{}}^{4}p_i \,\hat{\delta}^{(+)}(p_i^2-m_i^2)$([pdf](zotero://open-pdf/library/items/XIBRM7N7?page=8&annotation=8SCEXK6R))

\[image\] ([pdf](zotero://open-pdf/library/items/XIBRM7N7?page=8&annotation=2ZB9J7D5))  
([Kosower et al., 2019, p. 6](zotero://select/library/items/LFU9BUYS))

$\ket{\psi}_{\text{in}}=\int \dd{\Phi_2(p_1,p_2)} \phi_1(p_1) \phi_2(p_2) e^{\iunit b_\mu p^\mu_1 /\hbar}\ket{p_1,p_2}_\text{in}$ ([pdf](zotero://open-pdf/library/items/XIBRM7N7?page=8&annotation=ZRQPAZP4))  

## [The impulse on a particle](zotero://open-pdf/0_XIBRM7N7/9)

#definition : the impulse on a particle during  a scattering event: at the classical level, this is simply the total change in the momentum  of one of the particles — say particle 1 — during the collision. ([pdf](zotero://open-pdf/library/items/XIBRM7N7?page=9&annotation=E36J7HNG))

#condition : place detectors at symptotically large distances pointing at the collision region. ([pdf](zotero://open-pdf/library/items/XIBRM7N7?page=9&annotation=MG9CIJRQ))

#definition : $\mathbb{P_i^\mu}$ be the momentum operator for particle $i$ ([pdf](zotero://open-pdf/library/items/XIBRM7N7?page=9&annotation=9WSUGBYF))

#definition : $U(\infty,-\infty)$ is the time evolution operator from the far past to the far future ([pdf](zotero://open-pdf/library/items/XIBRM7N7?page=9&annotation=TRB4DKU4))

#important : we can think of the inserted states as the final state of a scattering process. ([pdf](zotero://open-pdf/library/items/XIBRM7N7?page=9&annotation=IY85BZHV))

#definition : X refers to any other particles which may be created ([pdf](zotero://open-pdf/library/items/XIBRM7N7?page=9&annotation=W565JUHN))

#important : The intermediate state  
containing X also necessarily contains exactly one particle each corresponding to fields 1  
and 2. ([pdf](zotero://open-pdf/library/items/XIBRM7N7?page=9&annotation=XIEIGS9Y))

#definition : momenta are denoted by r1,2 respectively ([pdf](zotero://open-pdf/library/items/XIBRM7N7?page=9&annotation=HV2FHSNL))

#definition : transition matrix T via S = 1 + iT, ([pdf](zotero://open-pdf/library/items/XIBRM7N7?page=10&annotation=9KNFYTLM))

\[image\] ([pdf](zotero://open-pdf/library/items/XIBRM7N7?page=10&annotation=W5JWG866))  
([Kosower et al., 2019, p. 8](zotero://select/library/items/LFU9BUYS))

#condition : Expanding the wavefunction in the first term, ([pdf](zotero://open-pdf/library/items/XIBRM7N7?page=10&annotation=WF2639Z6))

#important : re-inserted the final-state momenta ri in order to  
make manifest the phase independence of the result ([pdf](zotero://open-pdf/library/items/XIBRM7N7?page=10&annotation=VZY7YH9Z))

#important : independence ([pdf](zotero://open-pdf/library/items/XIBRM7N7?page=10&annotation=BNMP426G))

#condition : mo-  
mentum shifts qi = p0  
i − pi, and then change variables in the integration from the p0  
i to the  
qi ([pdf](zotero://open-pdf/library/items/XIBRM7N7?page=10&annotation=L45X2LTE))

#condition : perform the integral over q2 in eq. (3.22) using the four-fold delta function. ([pdf](zotero://open-pdf/library/items/XIBRM7N7?page=11&annotation=XWCU5HCB))

#condition : relabeling q1 → q, ([pdf](zotero://open-pdf/library/items/XIBRM7N7?page=11&annotation=KYU5QI24))

#important : Unusually for a physical observable, this contribution is linear in the amplitude. ([pdf](zotero://open-pdf/library/items/XIBRM7N7?page=11&annotation=F54JI7CC))

#important : The  
momentum q looks like a momentum transfer if we examine the amplitude alone, but for  
the physical scattering process it represents a difference between the momentum within  
the wavefunction and that in the conjugate. ([pdf](zotero://open-pdf/library/items/XIBRM7N7?page=11&annotation=YJZKEUF8))

\[image\] ([pdf](zotero://open-pdf/library/items/XIBRM7N7?page=11&annotation=EXD7I2F6))  
([Kosower et al., 2019, p. 9](zotero://select/library/items/LFU9BUYS))

#condition : we again introduce a complete set of  
states labelled by r1, r2 and X so that ([pdf](zotero://open-pdf/library/items/XIBRM7N7?page=11&annotation=NLSM2P3B))

#condition : As above, we can now expand the wavefunctions ([pdf](zotero://open-pdf/library/items/XIBRM7N7?page=11&annotation=IIAKXI2I))

#definition : rX denotes the total momentum carried by particles in X ([pdf](zotero://open-pdf/library/items/XIBRM7N7?page=11&annotation=RUJ2RY3P))

#definition : qi = p0  
i − p ([pdf](zotero://open-pdf/library/items/XIBRM7N7?page=12&annotation=GZKWQ4KH))

#condition : perform the integral over q2 ([pdf](zotero://open-pdf/library/items/XIBRM7N7?page=12&annotation=2DINVV5Q))

#condition : q1 → q to obtain, ([pdf](zotero://open-pdf/library/items/XIBRM7N7?page=12&annotation=9ZKTHMQT))

#definition : momentum transfers wi ≡ ri−pi ([pdf](zotero://open-pdf/library/items/XIBRM7N7?page=12&annotation=7CEEEI4W))

\[image\] ([pdf](zotero://open-pdf/library/items/XIBRM7N7?page=12&annotation=35V9JVAZ))  
([Kosower et al., 2019, p. 10](zotero://select/library/items/LFU9BUYS))

#definition : Let Kµ be the  
momentum operator for whatever field is radiated; ([pdf](zotero://open-pdf/library/items/XIBRM7N7?page=13&annotation=5ZAWVIHK))

#condition : introduce momentum transfers, qi = p0  
i−pi, and trade the integrals over p0  
i for  
integrals over the qi. One of the four-fold δ functions will again become ˆδ(4)(q1+q2), and we  
can use it to perform the q2 integrations. We again relabel q1 → q. ([pdf](zotero://open-pdf/library/items/XIBRM7N7?page=14&annotation=XKX2KUPI))

The integration leaves  
behind a pair of on-shell δ functions and positive-energy Θ functions just as in eqs. ( ([pdf](zotero://open-pdf/library/items/XIBRM7N7?page=14&annotation=RJ87ZXYE))

#important : As  
in eqs. (3.24) and (3.30), q represents a momentum mismatch rather than a momentum  
transfer ([pdf](zotero://open-pdf/library/items/XIBRM7N7?page=14&annotation=A79FPIYA))

#condition : change variables  
from the ri to make use of them ([pdf](zotero://open-pdf/library/items/XIBRM7N7?page=14&annotation=NGQKRM5B))

\[image\] ([pdf](zotero://open-pdf/library/items/XIBRM7N7?page=15&annotation=9V87AMWM))  
([Kosower et al., 2019, p. 13](zotero://select/library/items/LFU9BUYS))

#important : expectation of the radiated momentum is not completely independent of the impulse. ([pdf](zotero://open-pdf/library/items/XIBRM7N7?page=15&annotation=4UXMDEUG))

#definition : PPµ  
i is over all momentum operators in the theory ([pdf](zotero://open-pdf/library/items/XIBRM7N7?page=16&annotation=EP6TQXZF))

#important : The second equality  
above holds because Pµ  
i |ψi = 0 for i 6= 1, 2; only quanta of fields 1 and 2 are present in the  
incoming state. ([pdf](zotero://open-pdf/library/items/XIBRM7N7?page=16&annotation=J5MFJLQT))

#condition : total momentum is time independent ([pdf](zotero://open-pdf/library/items/XIBRM7N7?page=16&annotation=RAEJ3HUP))

\[image\] ([pdf](zotero://open-pdf/library/items/XIBRM7N7?page=16&annotation=XAKRFP55))  
([Kosower et al., 2019, p. 14](zotero://select/library/items/LFU9BUYS))

#important : first term hψ|i\[Pµ  
1 , T\]|ψi in the impulse (3.18) describes only the exchange of  
momentum between particles 1 and 2; in this sense it is associated with the classical  
Lorentz force (which shares this property) rather than with the classical ALD force (which ([pdf](zotero://open-pdf/library/items/XIBRM7N7?page=16&annotation=E6KDPHVD))

conservation of momentum at the level of expectation  
values is easy to demonstrate: ([pdf](zotero://open-pdf/library/items/XIBRM7N7?page=16&annotation=HJEZLHRP))

\[image\] ([pdf](zotero://open-pdf/library/items/XIBRM7N7?page=16&annotation=4YJN8V5M))  
([Kosower et al., 2019, p. 14](zotero://select/library/items/LFU9BUYS))

we explicitly see the net loss of momentum due to  
radiating messengers ([pdf](zotero://open-pdf/library/items/XIBRM7N7?page=17&annotation=TDJIJ4RM))

#important : quantity is suppressed by factors of g because of the additional  
state. ([pdf](zotero://open-pdf/library/items/XIBRM7N7?page=17&annotation=IYMCEPEL))

#important : As we approach the classical limit, these expectation values should  
reduce to the classical impulse and the classical radiated momentum. This should ensure  
that we are able to explore the ~ → 0 limit. ([pdf](zotero://open-pdf/library/items/XIBRM7N7?page=17&annotation=H3UAVPSU))

#important : We will take these to be wavepackets, characterized by a smearing or spread in  
momenta.2 ([pdf](zotero://open-pdf/library/items/XIBRM7N7?page=17&annotation=GSXF6HSC))

#condition : An example of a minimum-uncertainty wavefunction in momentum space (ignoring  
normalization) for a particle of mass m growing sharper in the ~ → 0 limit has the form, ([pdf](zotero://open-pdf/library/items/XIBRM7N7?page=17&annotation=NWAQC9KL))

\[image\] ([pdf](zotero://open-pdf/library/items/XIBRM7N7?page=17&annotation=ESHMNL2P))  
([Kosower et al., 2019, p. 15](zotero://select/library/items/LFU9BUYS))

#definition : \`c is the particle’s Compton wavelength ([pdf](zotero://open-pdf/library/items/XIBRM7N7?page=18&annotation=I278NUG2))

#definition : \`w is an additional parameter  
with dimensions of length ([pdf](zotero://open-pdf/library/items/XIBRM7N7?page=18&annotation=WS3Z344R))

#condition : Fourier-  
transforming, ([pdf](zotero://open-pdf/library/items/XIBRM7N7?page=18&annotation=MCRUS47A))

\[image\] ([pdf](zotero://open-pdf/library/items/XIBRM7N7?page=18&annotation=RAFJUZXY))  
([Kosower et al., 2019, p. 16](zotero://select/library/items/LFU9BUYS))

#important : \`w, which we could  
take as an intrinsic measure of the wavefunction’s spread ([pdf](zotero://open-pdf/library/items/XIBRM7N7?page=18&annotation=IJFUU269))

#important : the difference pi − hpµ  
i i  
may be spacelike ([pdf](zotero://open-pdf/library/items/XIBRM7N7?page=18&annotation=DHX7SF35))

#important : This means the wavefunction must  
depend on at least one four-vector parameter ([pdf](zotero://open-pdf/library/items/XIBRM7N7?page=18&annotation=JW3QC232))

#important : Iµ  
(1) (3.24)  
more closely. It has the form of an amplitude integrated over the on-shell phase space  
for both of the incoming momenta ([pdf](zotero://open-pdf/library/items/XIBRM7N7?page=18&annotation=PYP24WZ7))

#important : phase e  
−ib·q/~ dependent on the momentum mismatch q, ([pdf](zotero://open-pdf/library/items/XIBRM7N7?page=18&annotation=E3XX3R7T))

#important : integrated over all q. ([pdf](zotero://open-pdf/library/items/XIBRM7N7?page=19&annotation=DUSB9S3G))

#important : The amplitude will vary slowly  
on the scale of the wavefunction when one is close to the limit ([pdf](zotero://open-pdf/library/items/XIBRM7N7?page=19&annotation=7CGAULKX))

#condition : requiring  
the overlap to be O(1) is equivalent to requiring that φ∗(p + q) does not differ much from  
φ∗(p) ([pdf](zotero://open-pdf/library/items/XIBRM7N7?page=19&annotation=J8AMAXYC))

\[image\] ([pdf](zotero://open-pdf/library/items/XIBRM7N7?page=19&annotation=3V7QXJ55))  
([Kosower et al., 2019, p. 17](zotero://select/library/items/LFU9BUYS))

#condition : replacing the momentum by a wavenumber, ([pdf](zotero://open-pdf/library/items/XIBRM7N7?page=19&annotation=AFTXSKXC))

\[image\] ([pdf](zotero://open-pdf/library/items/XIBRM7N7?page=19&annotation=5FT3SM4Y))  
([Kosower et al., 2019, p. 17](zotero://select/library/items/LFU9BUYS))

#important : The integration over the initial momenta pi and the initial wavefunctions will smear out  
these delta functions to sharply peaked functions whose scale is the same order as the  
original wavefunctions. ([pdf](zotero://open-pdf/library/items/XIBRM7N7?page=19&annotation=RMXCC8JY))

#important : As ξ gets smaller, this function will turn back into a delta function  
imposed on the ¯q integration. I ([pdf](zotero://open-pdf/library/items/XIBRM7N7?page=19&annotation=YKI6U6FD))

#important : (The characteristic momentum mismatch q is necessarily spacelike.) ([pdf](zotero://open-pdf/library/items/XIBRM7N7?page=19&annotation=WI8JAZDR))

#definition : us call 1/p−q¯2 a  
‘scattering length’ \`s ([pdf](zotero://open-pdf/library/items/XIBRM7N7?page=19&annotation=6KGNS6SB))

#condition : The direction-averaging  
implicit in the integration over the pi ([pdf](zotero://open-pdf/library/items/XIBRM7N7?page=19&annotation=8RRG4BXK))

lead to a constraint on two positive quantities  
built out of the ratios, so in general we expect both to be constrained independently ([pdf](zotero://open-pdf/library/items/XIBRM7N7?page=19&annotation=K78PBHTS))

#important : If we had not already  
scaled out a factor of ~ from q, these constraints would make it natural to do so. ([pdf](zotero://open-pdf/library/items/XIBRM7N7?page=20&annotation=ZL7ET5D7))

#condition : Combining  
the second constraint with eq. (4.6), ([pdf](zotero://open-pdf/library/items/XIBRM7N7?page=20&annotation=P3RGSKNQ))

obtain the constraint \`w \` ([pdf](zotero://open-pdf/library/items/XIBRM7N7?page=20&annotation=5L4CWKYU))

\[image\] ([pdf](zotero://open-pdf/library/items/XIBRM7N7?page=20&annotation=Z5NYQSAW))  
([Kosower et al., 2019, p. 18](zotero://select/library/items/LFU9BUYS))

we should expect ¯q · ui to be smaller  
than, but still of order, √ξ/\`s. ([pdf](zotero://open-pdf/library/items/XIBRM7N7?page=20&annotation=3C7GKPA4))

#important : should take note of one additional length scale in the problem ([pdf](zotero://open-pdf/library/items/XIBRM7N7?page=20&annotation=7VHKSX9H))

\[image\] ([pdf](zotero://open-pdf/library/items/XIBRM7N7?page=20&annotation=A6HC4CER))  
([Kosower et al., 2019, p. 18](zotero://select/library/items/LFU9BUYS))

#important : There are really three dimensionless  
parameters we must consider: ξ; \`w/\`s; and ρcl/\`s. We want to retain the full dependence  
on the latter, while considering only effects independent of the first two ([pdf](zotero://open-pdf/library/items/XIBRM7N7?page=20&annotation=7VUPXMCL))