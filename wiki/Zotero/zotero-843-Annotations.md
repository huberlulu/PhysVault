* Mdnotes File Name: [[zotero-843]]
# Annotations  
(1/11/2022, 7:20:22 PM)

> N particles in 3-d space, for a total of 3N degrees of freedom  ([pdf](zotero://open-pdf/library/items/TAE9KIDZ?page=1&annotation=DM9PTLAJ))

^f4974d

> $L(q_i,\dot{q_{i}}):=T(\dot{q_i})-V(q_{i})$ ([pdf](zotero://open-pdf/library/items/TAE9KIDZ?page=1&annotation=B374YNPW))   ^323fd0


> T is the kinetic energy (that depends only on velocities $\dot{q_i}$)  ([pdf](zotero://open-pdf/library/items/TAE9KIDZ?page=1&annotation=Y23MCBQE))

^dd1fcc

> V is the potential energy (that depends only on positions $q_i$).  ([pdf](zotero://open-pdf/library/items/TAE9KIDZ?page=1&annotation=XGBVW5WQ))

^32ca1d

> we want to find $q_i(t)$ between those times, subject to the constraint that $q_i(t_1)$ and $q_i(t_2)$ are fixed at some known values.  ([pdf](zotero://open-pdf/library/items/TAE9KIDZ?page=1&annotation=H2Y2SFWF)) #

^645ab9

> infinite number of paths the system could take between these two times, and each path is specified by choosing the functions $q_i(t)$ (which in turn determines $\dot{q_i}$). Each choice of path gives a different form for the Lagrangian  ([pdf](zotero://open-pdf/library/items/TAE9KIDZ?page=1&annotation=6HZSCQK9))

^f2b5a1

> the action S, defined as a functional of the paths that can be followed, is an extremum  ([pdf](zotero://open-pdf/library/items/TAE9KIDZ?page=1&annotation=SLW8BABK))

^266b73

> \[image\] ([pdf](zotero://open-pdf/library/items/TAE9KIDZ?page=1&annotation=YWU2PSG5))  

^4370de


> The condition that S be an extremum is specified by requiring δS = 0  ([pdf](zotero://open-pdf/library/items/TAE9KIDZ?page=1&annotation=Y7XYFHFN))

^a3b30e

> Using the chain rule  ([pdf](zotero://open-pdf/library/items/TAE9KIDZ?page=2&annotation=APXBCKE9))

^b26c54

> \[image\] ([pdf](zotero://open-pdf/library/items/TAE9KIDZ?page=2&annotation=G2LIYEI7))  

^8311af


> integrate by parts  ([pdf](zotero://open-pdf/library/items/TAE9KIDZ?page=2&annotation=WMEG44CC))

^5b3394

>\[image\] ([pdf](zotero://open-pdf/library/items/TAE9KIDZ?page=2&annotation=DZQRTID8))  

^867192


>qi (t1) and qi (t2) are fixed means that δqi = 0 at the limits of integration, so the middle term is zero.  ([pdf](zotero://open-pdf/library/items/TAE9KIDZ?page=2&annotation=UMYNAEWA))

>\[image\] ([pdf](zotero://open-pdf/library/items/TAE9KIDZ?page=2&annotation=RPE3UJAK))  


>quantity in brackets must be zero  ([pdf](zotero://open-pdf/library/items/TAE9KIDZ?page=2&annotation=S72SR9KR))

>Euler-Lagrange equations  ([pdf](zotero://open-pdf/library/items/TAE9KIDZ?page=2&annotation=PI4TLXET))

>\[image\] ([pdf](zotero://open-pdf/library/items/TAE9KIDZ?page=2&annotation=H9IRQE86))  


>Why are position and velocity treated as independent?  ([pdf](zotero://open-pdf/library/items/TAE9KIDZ?page=2&annotation=BUK3TIV4))

>we can take qi and ̇ qi as independent variables  ([pdf](zotero://open-pdf/library/items/TAE9KIDZ?page=2&annotation=STSA2APZ))

^af8f23

>considering a simple case of an ordinary function in the 2-d x − y plane, such as y = x2  ([pdf](zotero://open-pdf/library/items/TAE9KIDZ?page=2&annotation=HBSBTL73))

^6e5c85

>suppose we define another function such as  ([pdf](zotero://open-pdf/library/items/TAE9KIDZ?page=3&annotation=HMA9K8CM))

^d3973c

>\[image\] ([pdf](zotero://open-pdf/library/items/TAE9KIDZ?page=3&annotation=C58CZUM3))  


>obtain different curves  ([pdf](zotero://open-pdf/library/items/TAE9KIDZ?page=3&annotation=ET429NCQ))

^80c6c3

>requiring f (x, y) to be equal to various constants  ([pdf](zotero://open-pdf/library/items/TAE9KIDZ?page=3&annotation=3EXYA8UK))

^934a08

>to obtain the specific curve y = x2, we must impose a specific constraint on L, namely that f (x, y) =  ([pdf](zotero://open-pdf/library/items/TAE9KIDZ?page=3&annotation=PZQWMGJU))

^1ebbbf

>same logic applies to the Lagrangian, except that the constraint is a bit more elaborate  ([pdf](zotero://open-pdf/library/items/TAE9KIDZ?page=3&annotation=C85VLISW))

^df626f

>If all we’re doing is specifying the position and velocity of a particle at one point, then qi and vi can take on any values so, at this stage, they are technically independent variables  ([pdf](zotero://open-pdf/library/items/TAE9KIDZ?page=3&annotation=5V5GLM5E))

>The constraint is to apply the principle of least action  ([pdf](zotero://open-pdf/library/items/TAE9KIDZ?page=3&annotation=QW829TNQ))

^bd2cff

>This leads to the Euler-Lagrange equations  ([pdf](zotero://open-pdf/library/items/TAE9KIDZ?page=3&annotation=JBHZFZ5M))

>now qi and ̇ qi are no longer independent, as ̇ qi (t) is just the derivative of qi  ([pdf](zotero://open-pdf/library/items/TAE9KIDZ?page=3&annotation=STQFB59R))

^91647f

>The connection of qi and ̇ qi emerges only after we solve the Euler-Lagrange equations.  ([pdf](zotero://open-pdf/library/items/TAE9KIDZ?page=3&annotation=ZFS23J69))

^64a08d

>Other types of Lagrangian  ([pdf](zotero://open-pdf/library/items/TAE9KIDZ?page=3&annotation=JNTHABJX))

>conceive of a Lagrangian that depended on qi, ̇ qi and ̈ qi  ([pdf](zotero://open-pdf/library/items/TAE9KIDZ?page=3&annotation=INC55476))

>The fact that physical Lagrangians depend only on qi, and ̇ qi is a consequence of Newton’s second law F = ma  ([pdf](zotero://open-pdf/library/items/TAE9KIDZ?page=3&annotation=GD7ENKKU))

>\[image\] ([pdf](zotero://open-pdf/library/items/TAE9KIDZ?page=4&annotation=C5URHCH7))  


>pi is the momentum of degree of freedom i  ([pdf](zotero://open-pdf/library/items/TAE9KIDZ?page=4&annotation=GTSDZ87X))

>EulerLagrange equations are equivalent to  ([pdf](zotero://open-pdf/library/items/TAE9KIDZ?page=4&annotation=LIHXUZH3))

>\[image\] ([pdf](zotero://open-pdf/library/items/TAE9KIDZ?page=4&annotation=GR263YNE))  


>This is just Newton’s second law, so the Euler-Lagrange formulation is indeed equivalent to Newton’s laws  ([pdf](zotero://open-pdf/library/items/TAE9KIDZ?page=4&annotation=NCBSLSDQ))

>2.CLASSICAL FIELD THEORY  ([pdf](zotero://open-pdf/library/items/TAE9KIDZ?page=4&annotation=DIDK3WTD))

>difference between particle theory and field theory is that the variables qi no longer describe the motion of anything, that is, they are no longer functions of time  ([pdf](zotero://open-pdf/library/items/TAE9KIDZ?page=4&annotation=XMYBE57W))

^11daed

>Rather, they become fixed labels for points in space.  ([pdf](zotero://open-pdf/library/items/TAE9KIDZ?page=4&annotation=LKY96CM6))

^c35748

>$q_i$ become independent variables  ([pdf](zotero://open-pdf/library/items/TAE9KIDZ?page=4&annotation=9H7MC5FF))

label points in spacetime  ([pdf](zotero://open-pdf/library/items/TAE9KIDZ?page=4&annotation=AYTCS4BF)) ^0246d7

A field is some quantity that has a value for each point in spacetime, and it is this quantity that can change as we move from place to place or forward in time  ([pdf](zotero://open-pdf/library/items/TAE9KIDZ?page=4&annotation=8TAHBHVN)) ^85f679

scalar field  ([pdf](zotero://open-pdf/library/items/TAE9KIDZ?page=4&annotation=XX3C3GCI))

consists of a single value φ (qμ) attached to each point in spacetime  ([pdf](zotero://open-pdf/library/items/TAE9KIDZ?page=4&annotation=9RYQCRIP)) ^96fc16

what is meant by a ’path’ that the system follows.  ([pdf](zotero://open-pdf/library/items/TAE9KIDZ?page=5&annotation=R8QSUBK8))

spacetime coordinates qμ are no longer dynamical variables  ([pdf](zotero://open-pdf/library/items/TAE9KIDZ?page=5&annotation=FH3JXDVI))

What does change is the value of the field φ  ([pdf](zotero://open-pdf/library/items/TAE9KIDZ?page=5&annotation=ZDB5BZMN))

φr if we have more than one field  ([pdf](zotero://open-pdf/library/items/TAE9KIDZ?page=5&annotation=8P63P39V))

path followed is determined by a function of the field values  ([pdf](zotero://open-pdf/library/items/TAE9KIDZ?page=5&annotation=WB9DHVRS)) ^92b120

define the Lagrangian density L (φr, φr,μ, qμ)  ([pdf](zotero://open-pdf/library/items/TAE9KIDZ?page=5&annotation=Q46S9A7S)) ^b6d267

φr,μ is defined as  ([pdf](zotero://open-pdf/library/items/TAE9KIDZ?page=5&annotation=7GDD8NQH)) ^05efbb

\[image\] ([pdf](zotero://open-pdf/library/items/TAE9KIDZ?page=5&annotation=T9C8MDAP))  


Lagrangian density is the Lagrangian per unit volume  ([pdf](zotero://open-pdf/library/items/TAE9KIDZ?page=5&annotation=PE963JZ5))

action element of this volume element between times t1 and t2 is  ([pdf](zotero://open-pdf/library/items/TAE9KIDZ?page=5&annotation=6XQRJMJE))

\[image\] ([pdf](zotero://open-pdf/library/items/TAE9KIDZ?page=5&annotation=NCZC8VNC))  


dS for only the volume element centred at qμ  ([pdf](zotero://open-pdf/library/items/TAE9KIDZ?page=5&annotation=HDY3777H)) ^0dacc9

total action  ([pdf](zotero://open-pdf/library/items/TAE9KIDZ?page=5&annotation=KBRM8M9B))

\[image\] ([pdf](zotero://open-pdf/library/items/TAE9KIDZ?page=5&annotation=T53J785B))  


apply to each field φr separately  ([pdf](zotero://open-pdf/library/items/TAE9KIDZ?page=5&annotation=THDR24AC))

summation over μ is implied  ([pdf](zotero://open-pdf/library/items/TAE9KIDZ?page=5&annotation=V5IXEK4X))

\[image\] ([pdf](zotero://open-pdf/library/items/TAE9KIDZ?page=5&annotation=554GJZHM))  


\[image\] ([pdf](zotero://open-pdf/library/items/TAE9KIDZ?page=6&annotation=75YHCWC5))  


product rule  ([pdf](zotero://open-pdf/library/items/TAE9KIDZ?page=6&annotation=734LBEEJ))

\[image\] ([pdf](zotero://open-pdf/library/items/TAE9KIDZ?page=6&annotation=4D6ARCVI))   ^d3367a


last term is the integral of a 4-d divergence over a 4-d volume  ([pdf](zotero://open-pdf/library/items/TAE9KIDZ?page=6&annotation=EAARESRK))

this integral goes to zero.  ([pdf](zotero://open-pdf/library/items/TAE9KIDZ?page=6&annotation=DB8ICX6W))

\[image\] ([pdf](zotero://open-pdf/library/items/TAE9KIDZ?page=6&annotation=6AADPT79))  


for all variations δφ  ([pdf](zotero://open-pdf/library/items/TAE9KIDZ?page=6&annotation=EX6NWWRS))

\[image\] ([pdf](zotero://open-pdf/library/items/TAE9KIDZ?page=6&annotation=LCQMGV6X))   ^caea84
