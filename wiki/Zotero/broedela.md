# Introduction to String Theory

![[broedela-zotero#Metadata]]

Other files:
* Mdnotes File Name: [[broedela]]
* Metadata File Name: [[broedela-zotero]]

##  Zotero links
* [Local library](zotero://select/items/1_8CFKRHKS)
* [Cloud library](http://zotero.org/users/7873466/items/8CFKRHKS)

## Notes
- [[broedela-0 Overview and Practicabilities]]
- [[broedela-1 Introduction and Motivation]]
- [[broedela-2 Point Particle actions]]
- [[broedela-3 Classical Bosonic String]]