---
alias: 
- contravariant vector
- contravariant vectors
- contravariantly
tag: GR SR
---
All the physical vectors $V$ should be invariant under [[Lorentz transformation]] and thus their *components* $V^\mu$ under a specific basis $\{e_\mu\}_\mu=\mathcal{B}$: $[V]^{\mathcal{B}}=[V]^{\mathcal{B}:\mu}[e_\mu]=V^\mu e_{(\mu)}$ should transform just like the [[Lorentz transformation#^spacetimeIntTrans|spacetime interval]] $\dd{x}^\mu$. Thus we define [[contravariant vectors in SR|contravariant vectors]], as any object whose components transform under a [[Lorentz transformation]] like:

$$V^\mu \rightarrow (V^\mu)'=\Lambda^\mu{}_\nu V^\nu $$
^definingProp

Some [[contravariant vectors in SR|contravariant vectors]] are the [[relativistic energy and momentum|momentum four-vector]] and the [[particle dynamics in SR#^relForce|relativistic force]]