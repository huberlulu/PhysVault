---
alias:
- scalar field
- scalar fields
- scalar
- scalars
tag: GR physics
---

A scalar field is a map from [[spacetime]] (manifold) to the reals, associating to every point $p$ in [[spacetime]] (manifold) $\mathcal{M}$ a number:
$$\begin{align}
\phi:\mathcal{M} &\rightarrow \mathbb{R}\\
p&\mapsto \phi(p)
\end{align}$$


Under a [[general coordinate transformation]]:

![[general coordinate transformation#^coordTrans]]

[[scalar fields in GR|Scalar fields]] do not transform:

$$\phi(p)\rightarrow \phi'(p)=\phi(p)$$