---
alias:
- PM
- PM expansion
- post-Minkowskian
tag:
- GR
- GW
---

The post-Minkowskian approach is a weak-field expansion in Newton’s constant, G^[![[@bernScalarQEDToy2021annotations#^e0eddb]]]. It maintains [[Lorentz group|Lorentz]] [[Tensors and tensor densities in General Relativity| invariance]] and gives results with exact relativistic velocity dependence ^[![[@bernScalarQEDToy2021annotations#^3c2880]]] and this meshes perfectly with [[scattering amplitude framework]].

IN the review:
[[@buonannoSourcesGravitationalWaves2015]]

# Length Scales

- [[compton wavelength]] $\lambda_c=\frac{\hbar}{m}$ where $m$ is the *mass scale*
- $r_S=Gm$ typical classical particle size (the [[Schwarzschild radius]])
- $b$ the interparticle separtation

# Classical expansion

Has the following hierarchy of scales: $\lambda_c \ll r_S \ll b$ ^[![[@bernScalarQEDToy2021annotations#^3427df]]] and implies a *regime of large charges* $\frac{r_S}{\lambda_c}\gg 1 \iff \frac{G m^2}{\hbar} \gg 1$^[![[@bernScalarQEDToy2021annotations#^684d37]]]


