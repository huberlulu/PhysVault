d# For [[Classical Particle Mechanics]]

For a N particles in 3D space parametrised by time, and $L[q_i,\dot{q_i}]$ the [[Lagrangian|lagrangian]], the [[Euler-Lagrange equations]] are:

$$\frac{\partial L}{\partial q_{i}}-\frac{d}{d t}\left(\frac{\partial L}{\partial \dot{q_{i}}}\right)=0$$

# For [[Classical Field Theory]]

$$\frac{\partial \mathcal{L}}{\partial \phi}-\frac{\partial}{\partial q^\mu}\left(\frac{\partial \mathcal{L}}{\partial \phi_{,\mu}}\right)=0$$

Where $\mathcal{L}$ is the [[Lagrangian Density]]