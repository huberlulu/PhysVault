The [[intrinsic worldsheet metric]] $g$ is the [[Strings|string]] analogue of the [[einbein]] $e$ for the point particle, which was considered as the (one-component) metric $g_{\tau \tau}=-e^2$^[[[broedela-3 Classical Bosonic String#^3687c1]]]. The [[intrinsic worldsheet metric]] is a [[symmetric tensor|symmetric]] 2-[[tensor]] on the [[worldsheet]]. A priori it is independent of the [[induced worldsheet metric|pullback metric]]^[[[broedela-3 Classical Bosonic String#^70afb4]]], and is in general curved. That is, there is a [[Levi-Civita connection]] and [[covariant derivative]] on the [[worldsheet]]. For a [[Field#Scalar Field|scalar field]] ($X^\mu$ is a [[worldsheet]] scalar), partial and [[covariant derivative]] agree.^[[[broedela-3 Classical Bosonic String#^ae356b]]]


# Varying the determinant
$$
\delta \sqrt{-\operatorname{det} g}=-\frac{1}{2} \sqrt{-\operatorname{det} g}  ~g_{\alpha \beta} \delta g^{\alpha \beta}=+\frac{1}{2} \sqrt{-\operatorname{det} g} ~g^{\alpha \beta} \delta g_{\alpha \beta}
$$