$x^{+}(\tau):=t(\tau)+z(\tau)=\alpha \tau$

Useful in some cases; prominent in [[String theory framework|string theory]]^[[[broedela-2 Point Particle actions#^bc85d3]]]
