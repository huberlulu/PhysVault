---
alias:
 - bosonic string
---

One possible [[String theory framework]] is that of the [[Bosonic String Theory|bosonic string]], where we consider particles with bosonic characteristic only. The [[Strings|strings]] will be parametrised with 2 variables and thus will swipe out a two-dimensional [[worldsheet]] on which the dynamics in the [[embedding space]] (or [[embedding space|ambient space]]) will be modelled^[[[broedela-3 Classical Bosonic String#^652893]]]. Crucially we consider the [[embedding space]] a [[Minkowski space]], with the [[metric]] $\eta_{\mu \nu}$    (c.f. [[Conventions|_|]]). ^13c909

The dynamics will be encoded in an [[Action|action]]. The [[Strings|string]] generalisation of the [[Worldline action for a relativistic point particle]] is the [[Nambu-Goto action|#|]] and the more quantisable [[Polynomial (einbein) Action]] analogue is the [[Polyakov action|#|]].

