$\dot{e}=0$

The [[Polynomial (einbein) Action#^8793e7|eom]] reduces to:

$$\ddot{X}=0$$ 

And the other [[Polynomial (einbein) Action#^8793e7|eom]] turns into a constraint^[[[broedela-2 Point Particle actions#^72f30d]]]: 

$$m^{2} e^{2}+\dot{X}^{2}=0$$