---
Alias:
- ambient space
- target space
---

Or Ambient space, is the real world space in which the particle or [[Strings|string]] lives. Usually this is $3+1$ dimensional [[spacetime]], but sometimes in [[String theory framework|string theory]], we work with $D+1$ dimensional [[spacetime]].

Reached by [[worldsheet#Embedding]]