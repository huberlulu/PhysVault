* ![[standard model#^stdmodel]]
* [Field quantisation introduces quanta: ](zotero://open-pdf/library/items/E2TKJSGY?page=7&annotation=GQQ9MLBC)
	* EM: photon, 
	* strong nuclear forces: gluons, 
	* gravity: [[graviton]], 
	* matter fields: electrons, quarks, neutrinos.
* Particle interaction is accessed by [[Feynman Graphs]]. ![[Feynman Graphs#^regpoint]]![[Feynman Graphs#^lab]]
* ![[standard model#^int]]
* [The fourth known force is lacking a quantum description](zotero://open-pdf/library/items/E2TKJSGY?page=7&annotation=ZZJ867I6) the two classical theories are: [[newtonian gravity]] and [[general relativity]]
* ![[quantum gravity#^int]] but [renormalization is a necessary requirement](zotero://open-pdf/library/items/E2TKJSGY?page=7&annotation=55FIDDU5) for physicality