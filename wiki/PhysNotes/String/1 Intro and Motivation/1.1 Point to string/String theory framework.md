---
Alias:
- string theory
- String theory
- string theory framework
---

- Particles are considered as [[Strings]]
- ![[Strings#^split]]
- ![[Strings#^excit]]
- ![[Strings#^space]]
* [the [[Strings|string]] sweeps out a *worldsheet* when moving in spacetime](zotero://open-pdf/library/items/E2TKJSGY?page=6&annotation=HWXRIPEB) whereas a [pointparticle, leaves a *worldline*](zotero://open-pdf/library/items/E2TKJSGY?page=6&annotation=WWGD7WYT)[![[worldlineandsheet]]](zotero://open-pdf/library/items/E2TKJSGY?page=6&annotation=LXJUMTKJ)
* There is a [classical descriptions of strings as well as quantum strings.](zotero://open-pdf/library/items/E2TKJSGY?page=7&annotation=ZM2HXN3T)
* There is [more 'freedom' in a string description](zotero://open-pdf/library/items/E2TKJSGY?page=7&annotation=7ATIWYQE)
* [Symmetries can be realized more efficiently](zotero://open-pdf/library/items/E2TKJSGY?page=7&annotation=9QENHRDE)
* [Divergencies and other mathematical imponderabilities](zotero://open-pdf/library/items/E2TKJSGY?page=7&annotation=56WFQD8X) [improves substantially, when considering strings.](zotero://open-pdf/library/items/E2TKJSGY?page=7&annotation=J8QPT8E9)



