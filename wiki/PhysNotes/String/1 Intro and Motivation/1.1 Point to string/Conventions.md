[Always set: ](zotero://open-pdf/library/items/E2TKJSGY?page=8&annotation=MK3NZ29K)$c=\hbar=e=1$, [units are then expressed in powers of ](zotero://open-pdf/library/items/E2TKJSGY?page=8&annotation=8YGEZQFF)$\text{kg}\sim\text{m}^{-1}$.
We use:
- [Minkowski spacetime](zotero://open-pdf/library/items/E2TKJSGY?page=8&annotation=M2L3ZLTB) $\mathbb{R}^{3,1}$
	- [Generalises to D spacetime dimensions>](zotero://open-pdf/library/items/E2TKJSGY?page=9&annotation=B3N2XI46) $\mathbb{R}^{D-1,1}$
-  [4-vectors](zotero://open-pdf/library/items/E2TKJSGY?page=9&annotation=4DAEI8EG) $x^{\mu}=(t,\vec{x})$; time $t=x^0$
	- [indices](zotero://open-pdf/library/items/E2TKJSGY?page=9&annotation=VQJT77H8) $\mu, \nu, ...=0,1,2,3$.
	- [summation convention:](zotero://open-pdf/library/items/E2TKJSGY?page=9&annotation=Z93GJHNL) $x^{\mu}y_{\mu}:=\sum\limits_{\mu=0}^{3}x^{\mu}y_{\mu}$
	- [Metric raises and lowers indices](zotero://open-pdf/library/items/E2TKJSGY?page=9&annotation=NKPJUNI2) $x_{\mu}:=\eta_{\mu \nu}x^\nu$
	- [Metric signature](zotero://open-pdf/library/items/E2TKJSGY?page=9&annotation=BCMCDLVE) $\eta_{\mu \nu}=\text{diag}(-+++)$, i.e. $p^2=-m^2$ ^5e02f0
	- [Dot products](zotero://open-pdf/library/items/E2TKJSGY?page=9&annotation=BN3LMA4U) are $x \cdot y:=\eta_{\mu\nu}x^{\mu}y^{\nu}$; squares $x^2:=x\cdot x$
- [[[Poincaré symmetry|Poincare symmetry]]: rotations, Lorentz boosts, spacial and temporal translations](zotero://open-pdf/library/items/E2TKJSGY?page=9&annotation=GCESLG3B)
- [Newton's constant G](zotero://open-pdf/library/items/E2TKJSGY?page=9&annotation=223C8W75)