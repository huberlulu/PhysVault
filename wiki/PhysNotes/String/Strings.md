---
alias:
- strings
- string
- String
---

*Strings* are [one-dimensional objects, which can be open or closed. ](zotero://open-pdf/library/items/E2TKJSGY?page=6&annotation=NS5KNIXE). They are one-dimensional extended object without inner structure, but with the possibility to oscillate.^[[[broedela-3 Classical Bosonic String#^024bce]]]
Open strings correspond to gauge theory, while closed states describe [[graviton|gravitons]]. [![[openandclosedstring]]](zotero://open-pdf/library/items/E2TKJSGY?page=6&annotation=LX9K6I3B)
 
### Tension 

^780334

Strings have one [*fundamental parameter T*, the string tension, which describes how easily a string can flex and/or vibrate](zotero://open-pdf/library/items/E2TKJSGY?page=6&annotation=4GJL389Z)^fundparam

Sometimes [inverse string tension is used: ](zotero://open-pdf/library/items/E2TKJSGY?page=8&annotation=6G93TWSJ) $$2\pi\alpha'=\frac{1}{T}.$$The parameter $\alpha'$ has dimensions $[\alpha']=[m^2]$ thus the [intrinsic scales of the theory are:](zotero://open-pdf/library/items/E2TKJSGY?page=8&annotation=IJP6JI2X) ^0f35e6
- string length: $l_s=2\pi\sqrt{\alpha'}$^stringlen
- string mass scale: $M_s=\frac{1}{\sqrt{\alpha'}}$^stringmass

The parameter $\alpha'$ is related to the fundamental string length scale^[[[broedela-3 Classical Bosonic String#^7baeb5]]]. Considering a time slice of the [[Nambu-Goto action|action]] (and thus a slice of the [[worldsheet]]), a slice of length $L$ leads to a potential$U \sim \frac{L}{\kappa^2}$ the constant force resulting from the potential is the string tension: $T=U'=\frac{1}{2 \pi \kappa^2}$^[[[broedela-3 Classical Bosonic String#^ce6098]]] Importantly this tension is constant: a spring or a rubber band would have a varying tension^[[[broedela-3 Classical Bosonic String#^d55a1e]]]. The string tension measures the resistance with respect displacement in orthogonal direction



### Excited strings 
Strings can oscillate, and are then referred to as [*excited*](zotero://open-pdf/library/items/E2TKJSGY?page=6&annotation=LNAIV8RT). [different masses of particles can be modelled by different types of oscillations](zotero://open-pdf/library/items/E2TKJSGY?page=6&annotation=RYGZMBBX) [called *modes*](zotero://open-pdf/library/items/E2TKJSGY?page=6&annotation=WFL6PQZA)^excit
### Splitting
[Strings can split and join, this leads to interaction](zotero://open-pdf/library/items/E2TKJSGY?page=6&annotation=VSBLP3SH)^split
### [[embedding space|Target space]] 
The movement of the string in [_[[embedding space|target space]]_ and its position are described by functions](zotero://open-pdf/library/items/E2TKJSGY?page=6&annotation=C4L3WA5H)  $X^\mu(\tau,\sigma)$ which are parametrised by a *[two-dimensional parameter](zotero://open-pdf/library/items/E2TKJSGY?page=6&annotation=HUFEQ2YU) space* with coordinates $\tau$ and $\sigma$^space
### String coupling
The *string coupling* $g_s$, [comes with string loops in the so-called genus expansion.](zotero://open-pdf/library/items/E2TKJSGY?page=8&annotation=KN4TLQQS)^coupling

