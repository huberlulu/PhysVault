---
alias:
- D-brane
- Dp-brane
- brane
- D-branes
- branes
- Branes
- Brane
---
![](https://cdn.mathpix.com/cropped/2022_01_21_c2f821e442f23993db05g-4.jpg?height=192&width=150&top_left_y=1432&top_left_x=540)

- D $p$-branes are $(p+1)$-dimensional submanifold of spacetime.
- The submanifold includes the time direction, it has signature $(p, 1)$.
- [[Dirichlet boundary conditions|Dirichlet conditions]] for the $D-p-1$ orthogonal directions of the submanifold.
- [[Neumann boundary conditions|Neumann conditions]] along the remaining $p+1$ directions along the submanifold.
- [[Dp-branes|D-branes]] can be curved submanifolds. 
- Open strings with pure [[Neumann boundary conditions|Neumann conditions]] can be viewed as a spacetime-filling $\mathrm{D}(D-1)$-[[Dp-branes|brane]].
- [[T-Duality]] maps between $\mathrm{D} p$-[[Dp-branes|branes]] and $\mathrm{D}(p \pm 1)$-[[Dp-branes|branes]].

Strings propagate on backgrounds with [[Dp-branes|D-branes]]:
- The bulk spacetime curvature governs the propagation of the string bulk,
- [[Dp-branes|D-branes]] govern the propagation of string ends.

There is even more to [[Dp-branes|D-branes]] as non-perturbative objects: We will continue this discussion later.