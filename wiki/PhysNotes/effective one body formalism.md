---
alias:
- EOB
- effective one body
tag:
 - GR
---

Initial paper:
[[@buonannoEffectiveOnebodyApproach1999]]

Review:
[[@buonannoSourcesGravitationalWaves2015]]