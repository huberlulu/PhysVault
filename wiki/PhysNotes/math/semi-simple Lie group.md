---
alias:
- semi-simple

tags:
- qftII
---
---
A [[semi-simple Lie group]] is a [[direct product]] of [[simple Lie group]]s.