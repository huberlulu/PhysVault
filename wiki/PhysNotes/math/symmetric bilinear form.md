





A [[symmetric bilinear form]] is exactly this, a [[bilinear form#^symmetric|symmetric]] [[bilinear form]].
# Inner product

One can define an [[inner product]] using a [[symmetric bilinear form]]. The [[inner product|Hermitian inner product]] reduces to this when not considering complex numbers.

# Signature
A [[symmetric bilinear form]] defines a [[quadratic form]], and one can thus define its signature, based on its [[bilinear form#Associated matrix|matrix representation]] $[B]$:


There always exists a change of variables given by an invertible matrix, not necessarily orthogonal, such that the eigenvalues of $[B]$,  $\lambda_{i}$ are $0,1$ and $-1$. Sylvester's law of inertia states that the numbers of each 1 and $-1$ are invariants of the quadratic form, in the sense that any other diagonalization will contain the same number of each. The **[[signature]] of the bilinear form** is the triple ( $n_{0}, n_{+}, n_{-}$), where $n_{0}$ is the number of $0$ s and $n_{\pm}$is the number of $\pm 1$s. 

We can identify the [[(semi)definiteness]] of the form from the [[(semi)definiteness#From signature|signature]]

A real vector space with an indefinite nondegenerate quadratic form of index $(p, q)$ (denoting $p 1$ s and $q-1 \mathrm{~s}$ ) is often denoted as $\mathbb{R}^{p, q}$ particularly in the physical theory of spacetime.

