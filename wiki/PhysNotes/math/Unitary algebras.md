---
alias:
tags:
- qftII
---
Among the simple Lie groups, the [[special unitary group]]s $\operatorname{SU}(N)$ have the simplest structure. Moreover, the groups with $N=2$ and $N=3$ along with the abelian group $\mathrm{U}(1)$ serve as the gauge groups of the standard model. Let us therefore discuss some features of the special unitary groups and their associated algebras.



Definition: [[su(n)]]

[[defining representation of su(n)]]
[[tensor product of su(n)]]
[[Young diagrams and Dynkin labels of su(n)]]
[[completness relations in su(n)]]
[[structure constants of su(n)]]
[[invariants of su(n)]]
[[su(2) and su(3)]]
