---
---

Generalized unitarity see ^[![[@brittoGeneralizedUnitarityOneloop2005]]]^[![[@bernOneloopNpointGauge1994]]]^[![[@bourjailyPrescriptiveUnitarity2017]]] is based on the factorization of amplitudes into simpler gauge-invariant on-shell building blocks which allows to export the simplicity of tree-amplitudes to loop-calculations^[![[@bernScalarQEDToy2021annotations#^c208cb]]]