---
alias: 
- unitary cut
tag:
- qft
---

Constructs loop-level scattering interands from lower-order gauge invariant on shell data^[![[@bernScalarQEDToy2021annotations#^708137]]]
