---
alias:
- canonically quantise
- quantise canonically
tags:
- qftII
---

Is to take the [[Poisson brackets|Poisson bracket]] formulation of the [[Hamiltonian]] equations and promote them to [[commutation relation]]s of [[operators]].

# canonical quantisation
-   Canonical quantisation is intrinsically not relativistically covariant due to the specialisation of time
-   Gauge fixing the massless vector field was not exactly fun.
-   Canonical quantisation is based on non-commuting field operators
-   treat interacting models perturbatively
-   deriving Feynman rules was a long effort
-   ordering ambiguities when quantising a classical expression

# For closed string:
[[covariant quantisation|canonical quantisation]]