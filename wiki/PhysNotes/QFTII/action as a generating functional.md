Using [[Feynman graphical notation]], we can write an action as a free term plus interaction vertices e.g;


![|300](file:///C:/Users/Lucien/Documents/UNI/images/image170.png)

where we used [[Feynman graphical notation#Functional derivative]]
